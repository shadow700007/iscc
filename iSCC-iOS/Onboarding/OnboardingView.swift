//
//  OnboardingView.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 13.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI
import iSCCCore

struct OnboardingView: View {
    var body: some View {
        ZStack {
            Image(sharedImage: .onboardingBacground)
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()

            VStack {
                BannerView()
                Text(sharedText: .movies)
                Text(sharedText: .most_watched)
            }


        }
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}
