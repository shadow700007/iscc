//
//  TabBarControllerView.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 17.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI

struct TabBarTemplateView<Content: View>: View {

    @State var destinationOne: String? = nil
    @State var destinationSecond: String? = nil
    @State var destinationThird: String? = nil
    @State var destinationFourth: String? = nil

    @State private var selectedTab: Int = 0

    private var content: () -> Content

    init(
        @ViewBuilder content: @escaping () -> Content
    ) {
        self.content = content
    }


    var body: some View {


        UITabBar.appearance().isHidden = true


        return
//            NavigationView {

                // TODO: Make it platform specific
                // IPad and macOS has the Side bar and possibly also Detail bar
//                #if os(macOS)
//
//                #else
//
//                #endif

                HStack {
//                    TabBarView()

                    content()

                }
    }
}


//struct TabBarControllerView_Previews: PreviewProvider {
//    static var previews: some View {
//        TabBarTemplateView()
//    }
//}
