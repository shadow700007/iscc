//
//  SyncTrakt.swift
//  StreamCinema.atv
//
//  Created by SCC on 04/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import TraktKit
import CoreData

final class SyncTrakt {
    var allObjects:[TraktKit.TraktHistoryItem] = []
    
    var date:Date = Date()
    
    private var completition:((_ progress: Float, _ error:Error?) -> Void)?
    
    func start(completition: ((_ progress: Float, _ error:Error?) -> Void)?) {
        self.completition = completition
        let lastSyncDate:Date? = self.lastSyncDate()
        self.startFetching(date:lastSyncDate)
    }
    
    func startFetching(at page:TraktKit.Pagination = TraktKit.Pagination(page: 1, limit: 1000), date: Date? = nil) {
        TraktManager.sharedManager.getHistory(startAt: date, endAt: Date(), pagination:page) { [weak self] traktObjects in
            guard let self = self else { return }
            switch traktObjects {
            case .success(objects: let objects, currentPage: let currentPage, limit: let limit):
                self.allObjects.append(contentsOf: objects)
                
                if limit > currentPage {
                    if let completition = self.completition {
                        let progress = Float(currentPage)/Float(limit)
                        DispatchQueue.main.async {
                            completition(progress,nil)
                        }
                    }
                    let page = TraktKit.Pagination(page: currentPage + 1, limit: page.limit)
                    self.startFetching(at:page, date: date)
                } else {
                    DispatchQueue.main.async {
                        self.finischFetching()
                    }
                }
            case .error(error: let error):
                if let completition = self.completition {
                    DispatchQueue.main.async {
                        completition(100,error)
                    }
                }
            }
        }
    }
    
    func finischFetching() {
        guard let managedContext = SyncTrakt.getContext() else { return }
        for item in self.allObjects {
            if item.type == "movie" {
                self.save(SCCMovie(traktItem: item), managedContext: managedContext, entity: .trakt)
            } else {
                self.save(SCCMovie(traktItem: item), managedContext: managedContext, entity: .trakt)
            }
        }
        self.setSyncDate(date: Date(), count:Int64(self.allObjects.count), context: managedContext)
        self.allObjects.removeAll()
        if let completition = self.completition {
            DispatchQueue.main.async {
                completition(100,nil)
            }
        }
    }
    
    public func deleteSyncDate() {
        if let persistentContainer = SyncTrakt.getContainer(),
           let context = SyncTrakt.getContext() {
            
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "TraktDate")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

            do {
                try persistentContainer.execute(deleteRequest, with: context)
                Log.write("Detele all data in TraktDate success")
            } catch let error as NSError {
                Log.write("Detele all data in TraktDate error : \(error.localizedDescription)")
            }
        }
    }
}

extension SyncTrakt {
    private static func getContext() -> NSManagedObjectContext? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.viewContext
    }
    
    private static func getContainer() -> NSPersistentStoreCoordinator? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.persistentStoreCoordinator
    }
    
    private func save(_ traktItem:SCCMovie, managedContext: NSManagedObjectContext, entity: WatchedEntity) {
        if !traktItem.existWithTraktID(context: managedContext, entity: entity) {
            traktItem.save(context: managedContext, entity: entity)
        }
    }

    private func lastSyncDate() -> Date? {
        guard let managedContext = SyncTrakt.getContext() else { return nil }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TraktDate")
        request.fetchLimit = 1
        
        let sort = NSSortDescriptor(key: "syncDate", ascending: false)
        request.sortDescriptors = [sort]
        
        do {
            let fetchData = try managedContext.fetch(request)
            if let data = fetchData.first as? NSManagedObject {
                let lastSync = TraktSyncDate(with: data)
                return lastSync.syncDate
            }
            
            print(fetchData)
        } catch  {
            Log.write("SyncTrakt lastSyncDate error : \(error.localizedDescription)")
        }
        return nil
    }
    
    private func setSyncDate(date: Date, count:Int64, context:NSManagedObjectContext) {
        let traktEntity = NSEntityDescription.insertNewObject(forEntityName: "TraktDate",
                                                              into: context)
        let sync = TraktSyncDate(syncDate: date, count: count)
        sync.updateObject(traktEntity)
        do {
            try context.save()
            Log.write("setSyncDate Success")
        } catch {
            Log.write("setSyncDate Error saving: \(error)")
        }
    }
}

final class TraktSyncDate: NSObject {
    var syncDate: Date
    var count: Int64
    
    init(syncDate: Date, count: Int64) {
        self.syncDate = syncDate
        self.count = count
    }
    
    init(with object: NSManagedObject) {
        self.syncDate = object.value(forKey: "syncDate") as? Date ?? Date(timeIntervalSince1970: 0)
        self.count = object.value(forKey: "count") as? Int64 ?? 0
    }
    
    func updateObject(_ object: NSManagedObject)  {
        object.setValue(self.syncDate, forKey: "syncDate")
        object.setValue(self.count, forKey: "count")
    }
}
