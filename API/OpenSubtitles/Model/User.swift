//
//  User.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

// MARK: - LoginResult
public struct OSLoginResult: Model {
    let user: OSUser?
    let token: String?
    let status: Int?
    
    static func empty<T>() -> T where T: Model {
        let login = OSLoginResult(user: nil, token: nil, status: nil)
        guard let emptyLogin = login as?T else {
            fatalError()
        }
        return emptyLogin
    }
}

// MARK: - User
struct OSUser: Model {
    let jti: String?
    let allowedDownloads: Int?
    let level: String?
    let userID: Int?
    let extInstalled, vip: Bool?

    enum CodingKeys: String, CodingKey {
        case jti
        case allowedDownloads
        case level
        case userID
        case extInstalled
        case vip
    }
    
    static func empty<T>() -> T where T: Model {
        let user = OSUser(jti: nil, allowedDownloads: nil, level: nil, userID: nil, extInstalled: nil, vip: nil)
        guard let emptyUser = user as?T else {
            fatalError()
        }
        return emptyUser
    }
}
