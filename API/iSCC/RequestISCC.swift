//
//  SC2_ApiRequest.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Alamofire
import Moya

extension Provider {
    static let isccRequest: MoyaProvider<RequestISCC> = NetworkingClient.provider()
}
//https://iscc.site/files/devices.php?deviceName=appleTV&deviceUUID=432423432423&appVersion=1.0.1&appBundle=zone.scc
//https://itsvet.net/KODI/ATV_SCC/files/devices.php?deviceName=appleTV&deviceUUID=432423432423&appVersion=1.0.1&appBundle=zone.scc
enum RequestISCC: TargetType, MoyaCacheable {
    
    case postData
    case getTvData
    case getEPGData(station:String)

    var cachePolicy: URLRequest.CachePolicy {
            switch self {
            case .getTvData:
                return .reloadIgnoringLocalCacheData
            default:
                return .useProtocolCachePolicy
            }
        }
    
    var baseURL: URL {
        return URLSettings.isccUrl
    }
    
    var method: Moya.Method {
        switch self {
        case .postData,
             .getTvData,
             .getEPGData:
            return .get
        }
    }
    
    var headers: [String: String]? {
        if let uuid = UIDevice.current.identifierForVendor?.uuidString,
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            let header = ["X-Uuid":uuid, "User-Agent":"com.streamcinemacommunity.atv" + "_" + appVersion]
            return header
        }
        return nil
    }
    
    var path: String {
        switch self {
        case .postData:
            return self.rootPath + "devices.php"
        case .getTvData:
            return self.rootPath + self.tvPath
        case .getEPGData:
            return self.rootPath + self.epgPath
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return URLEncoding.default
    }
    
    var task: Task {
        switch self {
        case .postData:
            guard let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
                  let appBundle = Bundle.main.infoDictionary?["CFBundleIdentifier"],
                  let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String,
                  let deviceUUID = UIDevice.current.identifierForVendor?.uuidString  else { return .requestPlain }
            let osVersion = UIDevice.current.systemVersion
            let deviceName = self.getSystemName()
            let version = appVersion + "-" + buildNumber
            return .requestParameters(parameters: ["deviceUUID":deviceUUID, "deviceName":deviceName, "appVersion":version, "appBundle":appBundle, "osVersion":osVersion], encoding: URLEncoding.queryString)
        case .getTvData:
            return .requestPlain
        case .getEPGData(station: let station):
            return .requestParameters(parameters: ["name":station], encoding: URLEncoding.queryString)
        }
    }
}

extension RequestISCC {
    var rootPath: String {
        return "/files/"
    }
    
    var tvPath: String {
        return "m3u8/playlist_iscc.json"
    }
    
    var epgPath: String {
        return "m3u8/getEpgForProgram.php"
    }
    
    private func getSystemName() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
}
