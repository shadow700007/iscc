//
//  HashPass.h
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

#ifndef HashPass_h
#define HashPass_h
#import <Foundation/Foundation.h>


@interface HashPass : NSObject 
+ (NSString *)md5Crypt:(NSString *)password
                  salt:(NSString *)salt
                prefix:(NSString *)prefix;
@end

#endif /* HashPass_h */
