//
//  FindService.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import Combine
import TraktKit

final class SC2Service {

    enum SC2Error: Error {
        case genreMovieFilterNotSpecified
        case getAllWatchedDataFailed
        case unknownTraktError
    }


    let apiClient = Provider.sc2Request

    //MARK: - Sources Endpoind
    public func setAsPlayed(mediaID: String) -> AnyPublisher<Void, Error> {
        apiClient
            .requestWithoutMapping(request: SC2Request.setAsPlayed(mediaID: mediaID))
            .map { _ in () }
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }
    
    public func sendAnalytics(url:URL) {
        guard let name = LoginManager.name else { return }
        apiClient
            .request(SC2Request.analytics(url: url, a: name))
    }

    public func getTvProgram(for station: String, date: String, page: Int) -> AnyPublisher<FilterResult, Error> {
        apiClient
            .request(request: SC2Request.tvProgram(station: station, date: date, page: page), model: FilterResult.self)
    }
    
    //NOT WORK! wrong api
    @available(*, deprecated)
    public func getMovie(tmdb: Int) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: .initTmdbFilter(value: tmdb))
    }
    
    //NOT WORK! wrong api
    @available(*, deprecated)
    public func getMovie(imdbID: String) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: .initImdbFilter(value: imdbID))
    }
    
    public func getMovies(traktID: [String], page: Int) -> AnyPublisher<SCCMovieResult, Error> {
        if traktID.count == 0 {
            return Just(SCCMovieResult())
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }
        return filter(with: .initTaktFilter(value: traktID, page: page, limit:traktID.count))
    }
    
    public func getStreams(for mediaID: String) -> AnyPublisher<VideoStreams, Error> {
        apiClient
            .requestArr(request: SC2Request.getStream(mediaID: mediaID), model: VideoStreams.self)
    }
    
    public func getMovieScc(by movieID:String, type: FilterType) -> AnyPublisher<SourceInfo, Error> {
        apiClient
            .request(request: SC2Request.getMedia(mediaID: movieID), model: SourceInfo.self)
    }

    public func getNext(episode:String, season: String, root_parent: String) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: .getNext(episode: episode, season: season, root_parent:root_parent))
    }
    
    public func getMovies(type: FilterType, for item: MenuItem, page: Int = 1, limit: Int? = nil) -> AnyPublisher<SCCMovieResult, Error> {
        switch item {
        case .dubbed:
            return filter(with: .lastReleasedDubbed(type: type, page: page, limit: limit))
        case .lastAdded:
            return filter(with: .lastAddedFilter(type: type, page: page, limit: limit))
        case .news:
            return filter(with: .newsFilter(type: type, page: page, limit: limit))
        case .popular:
            return filter(with: .popular(type: type, page: page, limit: limit))
        case .watched:
            return getWatched(type: type, page: page)
        case .watchedList:
            return getWatched(type: type, page: page, entity: .sccWatchList)
        case .mostWatched:
            return filter(with: .lastMostWatched(type: type, page: page, limit: limit))
        case .traktWatchList:
            return getTraktWatchList(page: page)
        case .trending:
            return filter(with: .trending(type: type, page: page, limit: limit))
        case .traktHistory:
            return getTraktHistori(page: page)
        case .genre:
            return Fail(error: SC2Error.genreMovieFilterNotSpecified).eraseToAnyPublisher()
        }
    }

    private func getTraktHistori(page: Int) -> AnyPublisher<SCCMovieResult, Error> {

        getHistoryTraktData()
            .flatMap { [weak self] ids -> AnyPublisher<SCCMovieResult, Error>  in
                guard let `self` = self else { return Fail(error: SC2Error.unknownTraktError).eraseToAnyPublisher() }
                return self.getMovies(traktID: ids, page: page)
            }
            .eraseToAnyPublisher()
    }
    
    private func getHistoryTraktData() -> AnyPublisher<[String], Error> {

        let subject: PassthroughSubject<[String], Error> = PassthroughSubject()

        TraktManager.sharedManager.getHistory { response in
            switch response {
            case .success(objects: let objects, currentPage: let currentPage, limit: let limit):
                Log.write("getHistory.success(objects: \(objects), currentPage: \(currentPage), limit: \(limit)")
                var ids:[String] = []
                for traktData in objects {
                    if let traktID = traktData.movie?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                    if let traktID = traktData.show?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                }
                subject.send(ids)
            case .error(error: let err):
                let error = err ?? SC2Error.unknownTraktError
                subject.send(completion: .failure(error))
            }
        }
        return subject.first().eraseToAnyPublisher()
    }

    private func getTraktWatchList(page: Int) -> AnyPublisher<SCCMovieResult, Error> {
        getWatchlistTraktData()
            .flatMap { [weak self] ids -> AnyPublisher<SCCMovieResult, Error> in
                guard let `self` = self else { return Fail(error: SC2Error.unknownTraktError).eraseToAnyPublisher() }
                return self.getMovies(traktID: ids, page: page)
            }.eraseToAnyPublisher()
    }
    
    private func getWatchlistTraktData() -> AnyPublisher<[String], Error>{

        let subject: PassthroughSubject<[String], Error> = PassthroughSubject()
        TraktManager.sharedManager.getWatchlist(watchType: .Movies) { response in
            switch response {
            case .error(error: let err):
                let error = err ?? SC2Error.unknownTraktError
                subject.send(completion: .failure(error))
            case .success(let objects, _, _):
                var ids:[String] = []
                for traktData in objects {
                    if let traktID = traktData.movie?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                    if let traktID = traktData.show?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                }
                subject.send(ids)
            }
        }
        return subject.first().eraseToAnyPublisher()
    }

    public func getSeries(for mediaID:String, showID: IDs?, seasonID: IDs?, page: Int, limit: Int?) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: FilterModel.seasonFilter(mediaID:mediaID, page: page, limit:limit))
            .map {
                var resultData = $0
                guard resultData.data.count > 0 else { return resultData }
                for i in 0...resultData.data.count - 1  {
                   if let showID = showID {
                       resultData.data[i].rootIDs = showID
                   }
                   if let seasonID = seasonID {
                       resultData.data[i].seasonIDs = seasonID
                   }
               }
             return resultData
            }.eraseToAnyPublisher()
    }
    
    public func getCsfdMedia(for csfdIDs:[String], type: FilterType, page: Int, sort:FilterSort? = nil) -> AnyPublisher<SCCMovieResult, Error>  {
        filter(with: FilterModel.csfdFillter(serviceID: csfdIDs, type: type, page: page, sort:sort))
    }
    
    public func getMedia(by ids:[String], type: FilterType, page: Int, sort:FilterSort? = nil) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: FilterModel.sccIDsFilter(sccIDs: ids, type: type, page: page, sort:sort, limit:ids.count))
    }

    private func getWatched(type: FilterType, page: Int, entity: WatchedEntity = .scc) -> AnyPublisher<SCCMovieResult, Error> {
        let data = WatchedData.getAllHistoryData(for: type, entity: entity)
        let items = Array(data.prefix(200))
        return getMedia(with: items, type: type, page: page, count:items.count)
    }
    
    private func getMedia(with data: [SCCMovie], type: FilterType, page: Int, count: Int) -> AnyPublisher<SCCMovieResult, Error> {
        if data.count == 0 {
            return Just(SCCMovieResult())
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()

        }
        var allSccIDs:[String] = []
        for watched in data {
            if let sccID = watched.rootIDs?.sccID {
                allSccIDs.append(sccID)
            }
//            if let sccID = watched.rootIDs?.sccID {
//                allSccIDs.append(sccID)
//            }
        }
        let filter = FilterModel.sccIDsFilter(sccIDs: allSccIDs, type: type, page: page, sort:nil, limit:50)
        let result = self.filter(with:filter)
        return result
    }

    public func genre(with genre:Genere, type:FilterType, page: Int) -> AnyPublisher<SCCMovieResult, Error> {
        let filterModel = FilterModel.genere(value: genre, type: type, limit: 30, page: page)
        return filter(with: filterModel)
    }
    
    //MARK: - FilterEndpoint
    public func customSearch(wtih model:CustomFilterModel) -> AnyPublisher<SCCMovieResult, Error> {
        apiClient
            .request(request: SC2Request.findMoviesBy(customFilter: model), model: FilterResult.self)
            .map { SCCMovieResult($0) }
            .eraseToAnyPublisher()
    }
    public func search(wtih model:FilterModel) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: model)
    }
    
    public func azSearch(model: FilterModel) -> AnyPublisher<AZResults, Error> {
        apiClient
            .request(request: SC2Request.azSearch(filter: model), model: AZResults.self)
    }

    private func filter(with model: FilterModel) -> AnyPublisher<SCCMovieResult, Error> {
        let request = SC2Request.filter(with: model)
        return apiClient.request(request: request, model: FilterResult.self)
                .map { SCCMovieResult($0) }
                .eraseToAnyPublisher()
    }
    
    private func analytics(_ path: String) {
        
    }
}


extension TraktManager.TraktError: Equatable {
    public static func == (lhs: TraktManager.TraktError, rhs: TraktManager.TraktError) -> Bool {
            return true // FIX
    }
}
