//
//  SourceInfoModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 28/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
// MARK: - Source
struct SourceInfo: DataConvertible, Hashable, Model {
    let parentID: String?
    let adult: Bool? //isNotExists?
    let dateAdded, country, language: String?
    let revenue: Int?
    let budget: Int?
    let playCount: Int?
    let infoLabels: InfoLabels?
    let i18NInfoLabels: [I18NInfoLabels]?
    let services: SupportedServices?
    let cast: [Cast]?
    let childrenCount: Int?
    let streamInfo: StreamInfo?
    let availableStreams: AvailableStreams?
    let videos: [VideoElement]?
    let ratings: [String:SCCRating]?
    let rootParent: String?
    
    func getAllRatings() -> [[String:String]] {
        var ratingsDict:[[String:String]] = []
        if let ratings = self.ratings {
            for item in ratings {
                if let currentRating = item.value.rating {
                    let rating:[String:String] = [item.key : "\(Int(currentRating * 10)) %"]
                    ratingsDict.append(rating)
                }
            }
        }
        return ratingsDict
    }
    
    func getMovieData() -> [[String:String]] {
        var data: [[String:String]] = []
        if let year = self.infoLabels?.year {
            data.append([String(localized: .year) : "\(year)"])
        }
        if let genre = self.infoLabels?.genre {
            let localizedGenere = genre.map({ name -> String in
                String(localizationKey: name)
            }).joined(separator: ", ")
            data.append([String(localized: .genre): localizedGenere])
        }
        if let durationInt = self.infoLabels?.duration {
            let duration = TimeInterval(durationInt)
            data.append([String(localized: .duration) : duration.formatedString() ?? ""])
        }
        
        if let audio = self.availableStreams?.languages?.audio?.map?.joined(separator: ", ") {
            data.append([String(localized: .lang) : audio])
        }
        if let srt = self.availableStreams?.languages?.subtitles?.map?.joined(separator: ", ") {
            data.append([String(localized: .sub) : srt])
        }
        return data
    }
    
    func getRating() -> String? {
        if let ratings = self.ratings {
            if let csfd = ratings["csfd"] {
                if let rating = csfd.rating {
                    return "\(Int(rating * 10)) %"
                }
            }
            for item in ratings {
                if let rating = item.value.rating {
                    return "\(item.key): \(Int(rating * 10)) %"
                }
            }
        }
        return nil
    }
    
    func getTrailer() -> String? {
        if var trailers = self.videos {
            trailers = trailers.filter { triler -> Bool in
                return (triler.url?.contains("youtube") ?? false)
            }
            for trailer in trailers {
                if let urlString = trailer.url {
                    return urlString
                }
            }
        }
        return nil
    }
    
    func getInfoLabels() -> I18NInfoLabels {
        let skLabel = self.i18NInfoLabels?.filter({ $0.lang == "sk" }).first
        let csLabel = self.i18NInfoLabels?.filter({ $0.lang == "cs" }).first
        let enLabel = self.i18NInfoLabels?.filter({ $0.lang == "en" }).first
        
        let currentLang = self.getCurrentLang()
        
        let title:String? = self.selectBestString(sk: skLabel?.title, cs: csLabel?.title, en: enLabel?.title, currentLang: currentLang)
        let plot:String? = self.selectBestString(sk: skLabel?.plot, cs: csLabel?.plot, en: enLabel?.plot, currentLang: currentLang)
        let plotoutline:String? = self.selectBestString(sk: skLabel?.plotoutline, cs: csLabel?.plotoutline, en: enLabel?.plotoutline, currentLang: currentLang)
        let trailer:String? = self.selectBestString(sk: skLabel?.trailer, cs: csLabel?.trailer, en: enLabel?.trailer, currentLang: currentLang)
        let tagline:String? = self.selectBestString(sk: skLabel?.tagline, cs: csLabel?.tagline, en: enLabel?.tagline, currentLang: currentLang)
        var parentTitles:[String?] = []
        parentTitles.append(contentsOf: (skLabel?.parentTitles ?? []))
        parentTitles.append(contentsOf: (csLabel?.parentTitles ?? []))
        parentTitles.append(contentsOf: (enLabel?.parentTitles ?? []))
        
        let poster:String? = self.selectBestString(sk: skLabel?.art?.poster, cs: csLabel?.art?.poster, en: enLabel?.art?.poster, currentLang: currentLang)
        let fanart:String? = self.selectBestString(sk: skLabel?.art?.fanart, cs: csLabel?.art?.fanart, en: enLabel?.art?.fanart, currentLang: currentLang)
        let thumb:String? = self.selectBestString(sk: skLabel?.art?.thumb, cs: csLabel?.art?.thumb, en: enLabel?.art?.thumb, currentLang: currentLang)
        let banner:String? = self.selectBestString(sk: skLabel?.art?.banner, cs: csLabel?.art?.banner, en: enLabel?.art?.banner, currentLang: currentLang)
        let clearart:String? = self.selectBestString(sk: skLabel?.art?.clearart, cs: csLabel?.art?.clearart, en: enLabel?.art?.clearart, currentLang: currentLang)
        let clearlogo:String? = self.selectBestString(sk: skLabel?.art?.clearlogo, cs: csLabel?.art?.clearlogo, en: enLabel?.art?.clearlogo, currentLang: currentLang)
        let landscape:String? = self.selectBestString(sk: skLabel?.art?.landscape, cs: csLabel?.art?.landscape, en: enLabel?.art?.landscape, currentLang: currentLang)
        let icon:String? = self.selectBestString(sk: skLabel?.art?.icon, cs: csLabel?.art?.icon, en: enLabel?.art?.icon, currentLang: currentLang)
        let art:Art = Art(poster: poster, fanart: fanart, thumb: thumb, banner: banner, clearart: clearart, clearlogo: clearlogo, landscape: landscape, icon: icon)
        
        let info = I18NInfoLabels(title: title,
                                   lang: nil,
                                   parentTitles: parentTitles,
                                   plot: plot,
                                   plotoutline: plotoutline,
                                   trailer: trailer,
                                   tagline: tagline,
                                   art: art)
        return info
    }
    
    private func getCurrentLang() -> String {
        var currentLang = "en"
        if let system = Locale.preferredLanguages.first,
            let lang = system.lowercased().split(separator: "-").first {
            currentLang = String(lang)
        }
        return currentLang
    }
    
    private func selectBestString(sk:String?, cs:String?, en:String?, currentLang:String) -> String? {
        var langs:[String] = ["sk", "cs", "en"]
        
        guard let index = langs.firstIndex(where: { $0 == currentLang }) else { return en }
        let element = langs.remove(at: index)
        langs.insert(element, at: 0)
        
        let bestStringDict:[String:String?] = ["sk":sk, "cs":cs, "en":en]
        
        for lang in langs {
            if let selectedString = bestStringDict[lang] as? String, !selectedString.isEmpty {
                return selectedString
            }
        }
        
        return en
    }
    
    enum CodingKeys: String, CodingKey {
        case parentID = "parent_id"
        case adult
        case dateAdded = "date_added"
        case country, language, revenue, budget, videos, ratings
        case rootParent = "root_parent"
        case infoLabels = "info_labels"
        case i18NInfoLabels = "i18n_info_labels"
        case cast = "cast"
        case services
        case playCount = "play_count"
        case childrenCount = "children_count"
        case availableStreams = "available_streams"
        case streamInfo = "stream_info"
    }
    
    static func empty<T>() -> T where T: Model {
        let source = SourceInfo(parentID: "", adult: false, dateAdded: "", country: "", language: "", revenue: 0, budget: 1, playCount: 1 ,infoLabels: InfoLabels.empty(), i18NInfoLabels: [], services: SupportedServices.empty(),cast: [],childrenCount:0, streamInfo: nil, availableStreams: nil, videos: [], ratings: [:], rootParent: nil)
        guard let emptySource = source as?T else {
            fatalError()
        }
        return emptySource
    }
}

struct SCCRating: Hashable, Model {
    let rating: Double?
    let votes: Int?
    
    static func empty<T>() -> T where T : Model {
        let model = SCCRating(rating: nil, votes: nil)
        guard let emptyModel = model as?T else {
            fatalError()
        }
        return emptyModel
    }
}


struct VideoElement: Hashable, Model {
    let type: String?
    let url: String?
    let lang: String?
    
    enum CodingKeys: String, CodingKey {
        case type, url, lang
    }
    
    static func empty<T>() -> T where T : Model {
        let model = VideoElement(type: nil, url: nil, lang: nil)
        guard let emptyModel = model as?T else {
            fatalError()
        }
        return emptyModel
    }
}
