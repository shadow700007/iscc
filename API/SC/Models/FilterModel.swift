//
//  FilterModel.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

struct CustomFilterModel {
    var sortConfig: String
    var config: String
    
    init(sortConfig: String, config: String) {
        self.sortConfig = sortConfig
        self.config = config
    }

    init(with castName:String) {
        sortConfig = "{\"info_labels.premiered\":{\"order\" : \"asc\"}}"
        ///{"bool":{"must":[{"match":{"cast.name":"Tom Cruise"}}], "should":[{"match":{"info_labels.mediatype":"movie"}},{"match":{"info_labels.mediatype":"tvshow"}}]}}
        ///"{\"bool\":{\"must\":[{\"match\":{\"cast.name\":\"\(castName)\"}}]}}"
        config = "{\"bool\":{\"must\":[{\"match\":{\"cast.name\":\"\(castName)\"}}], \"should\":[{\"match\":{\"info_labels.mediatype\":\"movie\"}},{\"match\":{\"info_labels.mediatype\":\"tvshow\"}}]}}"
    }
}

struct FilterModel {
    var name:  FilterName
    var type:  FilterType
    var order: FilterOrder
    var sort:  FilterSort?
    
    var count: FilterCount?
    var lang: [String]?
    var value: [String]?
    var service: FilterService?
    var days: String?
    var limit: Int?
    var page: Int?
    var genre: Genere?
    var episode: String?
    var season: String?
    var root_parent: String?
    
    static func initTaktFilter(value: [String], page: Int, limit: Int? = nil) -> FilterModel {
        let filter = FilterModel(name: .service, type: .all, order: .desc, sort: nil, count:nil, lang: nil, value: value, service: .traktWithType, days: nil, limit: limit, page: page)
        return filter
    }
    
    static func initTmdbFilter(value: Int, limit:Int? = nil) -> FilterModel {
        let filter = FilterModel(name: .service, type: .all, order: .desc, sort: nil, count:nil, lang: nil, value: ["\(value)"], service: .tmdb, days: nil, limit: limit, page: nil)
        return filter
    }
    
    static func initImdbFilter(value: String, limit:Int? = nil) -> FilterModel {
        let filter = FilterModel(name: .service, type: .all, order: .desc, sort: nil, count:nil, lang: nil, value: [value], service: .imdb, days: nil, limit: limit, page: nil)
        return filter
    }
    
    static func azInit(type: FilterType = .movie, value: String? = nil, page: Int, limit:Int? = nil) -> FilterModel {
        var newValue:[String] = []
        if value != nil {
            newValue.append(value!)
        }
        let filter = FilterModel(name: .startsWithSimple, type: type, order: .desc, sort: .title, count: .titles, lang: nil, value: newValue, service: nil, days: nil, limit: limit, page: page)
        return filter
    }
    
    static func searchStartSimpleInit(type: FilterType = .movie, value: String? = nil, limit: Int? = nil, page: Int) -> FilterModel {
        var newValue:[String] = []
        if value != nil {
            newValue.append(value!)
        }
        let filter = FilterModel(name: .startsWithSimple, type: type, order: .desc, sort: nil, count: nil, lang: nil, value: newValue, service: nil, days: nil, limit: limit, page: page)
        return filter
    }
    
    static func genere(value:Genere, type:FilterType, limit: Int = 100, page: Int) -> FilterModel {
        return FilterModel(name: .genre,
                           type: type,
                           order: .desc,
                           sort: .trending,
                           count: nil,
                           lang: nil,
                           value: [value.rawValue],
                           service: nil,
                           days: nil,
                           limit: limit,
                           page: page)
    }
    
    static func allFillter(value:String, limit: Int = 100, page: Int) -> FilterModel {
        return FilterModel(name: .search,
                           type: .all,
                           order: .desc,
                           sort: .score,
                           count: nil,
                           lang: nil,
                           value: [value],
                           service: nil,
                           days: nil,
                           limit: limit,
                           page: page)
    }
    
    static func sccIDsFilter(sccIDs:[String], type: FilterType, page: Int, sort:FilterSort? = nil, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .ids,
                           type: type,
                           order: .desc,
                           sort: sort,
                           count: nil,
                           lang: nil,
                           value: sccIDs,
                           service: nil,
                           days: nil,
                           limit: limit,
                           page: page,
                           genre: nil)
    }
    
    static func csfdFillter(serviceID:[String], type: FilterType, page: Int, sort:FilterSort? = nil, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .service,
                           type: type,
                           order: .desc,
                           sort: sort,
                           count: nil,
                           lang: nil,
                           value: serviceID,
                           service: .csfd,
                           days: nil,
                           limit: limit,
                           page: page)
    }
    
    static func seasonFilter(mediaID:String, page: Int, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .parent,
                           type: .season,
                            order: .asc,
                            sort: .episode,
                            count: nil,
                            lang: nil,
                            value: [mediaID],
                            service: nil,
                            days: nil,
                            limit: limit,
                            page: page)
    }
    
    static func popularFilter(type: FilterType, page: Int, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .all,
                                type: type,
                                order: .desc,
                                sort: .playCount,
                                count: nil,
                                lang: nil,
                                value: nil,
                                service: nil,
                                days: "365",
                                limit: limit,
                                page: page)
    }
    
    static func newsFilter(type: FilterType, page: Int, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .news,
                            type: type,
                            order: .desc,
                            sort: .dateAdded,
                            count: nil,
                            lang: nil,
                            value: nil,
                            service: nil,
                            days: "365",
                            limit: limit,
                            page: page)
    }
    
    static func lastAddedFilter(type: FilterType, page: Int, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .all,
                            type: type,
                            order: .desc,
                            sort: .dateAdded,
                            count: nil,
                            lang: nil,
                            value: nil,
                            service: nil,
                            days: nil,
                            limit: limit,
                            page: page)
    }
    
    static func trending(type: FilterType, page: Int, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .all,
                        type: type,
                        order: .desc,
                        sort: .trending,
                        count: nil,
                        lang: nil,
                        value: nil,
                        service: nil,
                        days: nil,
                        limit: limit,
                        page: page)
    }
    
    static func popular(type: FilterType, page: Int, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .all,
                        type: type,
                        order: .desc,
                        sort: .popularity,
                        count: nil,
                        lang: nil,
                        value: nil,
                        service: nil,
                        days: nil,
                        limit: limit,
                        page: page)
    }
    
    static func lastMostWatched(type: FilterType, page: Int, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .all,
                        type: type,
                        order: .desc,
                        sort: .playCount,
                        count: nil,
                        lang: ["cs","sk"],
                        value: nil,
                        service: nil,
                        days: "7",
                        limit: limit,
                        page: page)
    }
    
    static func lastReleasedDubbed(type: FilterType, page: Int, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .newsDubbed,
                        type: type,
                        order: .desc,
                        sort: .langDateAdded,
                        count: nil,
                        lang: ["cs","sk"],
                        value: nil,
                        service: nil,
                        days: "730",
                        limit: limit,
                        page: page)
    }
    
    static func getNext(episode:String, season:String, root_parent:String, limit:Int? = nil) -> FilterModel {
        return FilterModel(name: .numbering,
                           type: .all,
                        order: .desc,
                        sort: nil,
                        count: nil,
                        lang: nil,
                        value: nil,
                        service: nil,
                        days: nil,
                        limit: limit,
                        page: nil,
                        episode: episode,
                        season: season,
                        root_parent: root_parent)
    }
    
    func getDictionnary() -> [String:Any]{
        var dict: [String: Any] = [:]

        dict["order"] = self.order.rawValue
        dict["type"] = self.type.rawValue
        
        if self.sort != nil {
            dict["sort"] = self.sort!.rawValue
        }
        if let lang = self.lang {
            dict["lang"] = lang
        }
        if self.value != nil,
           self.name != .ids {
            dict["value"] = self.value
        } else if self.name != .ids {
            dict["value"] = ""
        } else if self.name == .ids {
            dict["id"] = self.value
        }
        if self.service != nil {
            dict["service"] = self.service?.rawValue
        }
        if self.days != nil {
            dict["days"] = self.days
        }
        if self.limit != nil {
            dict["limit"] = self.limit
        }
        
        if let token = Bundle.main.infoDictionary?["CFBundleSCC"]  {
            dict["access_token"] = token
        }
        
        if let page = self.page {
            dict["page"] = page
        }
        
        if let season = self.season {
            dict["season"] = season
        }
        
        if let episode = self.episode {
            dict["episode"] = episode
        }
        
        if let root_parent = self.root_parent {
            dict["root_parent"] = root_parent
        }
        
        return dict
    }
    
}

enum FilterService: String {
    case csfd
    case imdb
    case traktWithType = "trakt_with_type"
    case tvdb
    case slug
    case tmdb
    case fanart
    case trakt
}

enum FilterName: String {
    case search
    case startsWithSimple
    case genre
    case all
    case service
    case ids
    case parent
    case studio
    case newsDubbed
    case news
    case country
    case year
    case language
    case numbering
    case concert
}

enum FilterSort: String {
    case score
    case year
    case premiered
    case dateAdded
    case title
    case playCount
    case lastSeen
    case episode
    case news
    case popularity
    case trending
    case langDateAdded

}

enum FilterOrder: String {
    case asc
    case desc
}

enum FilterType: String {
    case movie
    case tvshow
    case concert
    case season
    case episode
    case all = "*"
    case trakt
}

enum FilterCount: String {
    case titles
    case genres
    case studios
    case countries
}
