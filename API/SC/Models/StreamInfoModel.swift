//
//  MediaModel.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

// MARK: - StreamInfo
struct StreamInfo: DataConvertible, Hashable, Model {
    let audio: Audio?
    let video: Video?
    let subtitles: Subtitles?
    
    static func empty<T>() -> T where T: Model {
        let empty = StreamInfo(audio: nil, video: nil, subtitles: nil)
        guard let emptyE = empty as?T else {
            fatalError()
        }
        return emptyE
    }
}

// MARK: - Audio
struct Audio: DataConvertible, Hashable, Model {
    let language, codec: String?
    let channels: Int?
    
    static func empty<T>() -> T where T: Model {
        let empty = Audio(language: nil, codec: nil, channels: nil)
        guard let emptyE = empty as?T else {
            fatalError()
        }
        return emptyE
    }
}

// MARK: - Subtitles
struct Subtitles: DataConvertible, Hashable, Model {
    let language: String?
    
    static func empty<T>() -> T where T: Model {
        let empty = Subtitles(language: nil)
        guard let emptyE = empty as?T else {
            fatalError()
        }
        return emptyE
    }
}

// MARK: - Video
struct Video: DataConvertible, Hashable, Model {
    let width, height: Int?
    let codec: String?
    let aspect, duration: Double?
    
    static func empty<T>() -> T where T: Model {
        let empty = Video(width: nil, height: nil, codec: nil, aspect: nil, duration: nil)
        guard let emptyE = empty as?T else {
            fatalError()
        }
        return emptyE
    }
}

struct AvailableLanguages: Hashable, Model {
    let map: [String]?
    
    static func empty<T>() -> T where T: Model {
        let language = AvailableLanguages(map: [])
        guard let emptyLanguage = language as?T else {
            fatalError()
        }
        return emptyLanguage
    }
}

struct AudioLang: DataConvertible, Hashable, Model {
    let audio, subtitles: AvailableLanguages?

    enum CodingKeys: String, CodingKey {
        case audio
        case subtitles
    }
    
    static func empty<T>() -> T where T: Model {
        let language = AudioLang(audio: nil, subtitles: nil)
        guard let emptyLanguage = language as?T else {
            fatalError()
        }
        return emptyLanguage
    }
}

struct AvailableStreams: DataConvertible, Hashable, Model {
    let count: Int?
    let languages: AudioLang?

    func getAudio() -> [Language] {
        if let langs = self.languages?.audio?.map {
            let langUniques = Array(Set(langs))
            var languages: [Language] = []
            for lang in langUniques {
                if let language = Language(rawValue: lang) {
                    languages.append(language)
                }
            }
            return languages
        }
        return []
    }
    
    func getSubtitles() -> [Language] {
        if let langs = self.languages?.subtitles?.map {
            let langUniques = Array(Set(langs))
            var languages: [Language] = []
            for lang in langUniques {
                if let language = Language(rawValue: lang) {
                    languages.append(language)
                }
            }
            return languages
        }
        return []
    }
    
    enum CodingKeys: String, CodingKey {
        case count
        case languages
    }
    
    static func empty<T>() -> T where T: Model {
        let streams = AvailableStreams(count: nil, languages: nil)
        guard let emptyStreams = streams as?T else {
            fatalError()
        }
        return emptyStreams
    }
}

public enum Language:String {
    case sk
    case cs
    case en
    case de
    case hu
    
    var name: String {
        switch self {
        case .sk:
            return "SK"
        case .cs:
            return "CZ"
        case .en:
            return "EN"
        case .de:
            return "DE"
        case .hu:
            return "HU"
        }
    }
}

// MARK: - Cast
struct Cast: DataConvertible, Hashable, Model {
    let name, role, thumbnail: String?
    let order: Int?
    
    static func empty<T>() -> T where T: Model {
        let cast = Cast(name: "", role: "", thumbnail: "", order: 0)
        guard let emptyCast = cast as?T else {
            fatalError()
        }
        return emptyCast
    }
}

extension TimeInterval{

    func minutesString() -> String {

        let time = NSInteger(self)
        let minutes = (time / 60)
        return String(format: "%d min",minutes)

    }
}

struct I18NInfoLabels: DataConvertible, Hashable, Model {
    let title: String?
    let lang: String?
    let parentTitles: [String?]?
    let plot, plotoutline: String?
    let trailer: String?
    let tagline: String?
    let art: Art?

    enum CodingKeys: String, CodingKey {
        case title, lang
        case parentTitles = "parent_titles"
        case plot, plotoutline, trailer, tagline, art
    }
    
    static func empty<T>() -> T where T: Model {
        let infoLabels = I18NInfoLabels(title: "",
                                        lang: "",
                                 parentTitles: [],
                                 plot: "",
                                 plotoutline: "",
                                 trailer: "",
                                 tagline: "",
                                 art: Art.empty())
        guard let emptyInfo = infoLabels as?T else {
            fatalError()
        }
        return emptyInfo
    }
}

// MARK: - Art
struct Art: DataConvertible, Hashable, Model {
    let poster, fanart, fanartOirg, thumb, banner: String?
    let clearart, clearlogo, landscape, icon: String?
    
    init(poster: String?, fanart: String?, thumb: String?, banner: String?, clearart: String?, clearlogo: String?, landscape: String?, icon: String?) {
        self.fanartOirg = fanart
        if let post = poster, !post.isEmpty,
            !post.contains("poster-free.png") {
            self.poster = Art.thumbnail(from: post)
        } else {
            self.poster = nil
        }
        if let fanart = fanart, !fanart.isEmpty {
            self.fanart = fanart
        } else {
            self.fanart = nil
        }
        if let thumb = thumb, !thumb.isEmpty {
            self.thumb = Art.thumbnail(from: thumb)
        } else {
            self.thumb = nil
        }
        if let banner = banner, !banner.isEmpty {
            self.banner = Art.bannerThumb(from: banner)
        } else {
            self.banner = nil
        }
        if let clearart = clearart, !clearart.isEmpty {
            self.clearart = Art.thumbnail(from: clearart)
        } else {
            self.clearart = nil
        }
        if let clearlogo = clearlogo, !clearlogo.isEmpty {
            self.clearlogo = Art.thumbnail(from: clearlogo)
        } else {
            self.clearlogo = nil
        }
        if let landscape = landscape, !landscape.isEmpty {
            self.landscape = Art.thumbnail(from: landscape)
        } else {
            self.landscape = nil
        }
        if let icon = icon, !icon.isEmpty {
            self.icon = Art.thumbnail(from: icon)
        } else {
            self.icon = nil
        }
    }
    
    var isEmpty:Bool {
        if self.getPoster() != nil {
            return false
        }
        return true
    }
    
    func getPoster() -> String? {
        if let post = self.poster,
            !post.contains("poster-free.png") {
            return Art.thumbnail(from: post)
        }
        if thumb != nil {
            return Art.thumbnail(from: thumb!)
        }
        if fanart != nil {
            return Art.thumbnail(from: fanart!)
        }
        if banner != nil{
            return Art.bannerThumb(from: banner!)
        }
        return nil
    }
    
    func getFanArt() -> String? {
        if fanart != nil {
            return Art.thumbnail(from: fanart!)
        }
        if let post = self.poster,
            !post.contains("poster-free.png") {
            return Art.thumbnail(from: post)
        }
        
        if thumb != nil {
            return Art.thumbnail(from: thumb!)
        }
        
        if banner != nil{
            return Art.bannerThumb(from: banner!)
        }
        return nil
    }
    
    private static func bannerThumb(from urlString:String) -> String {
        let url = urlString.replacingOccurrences(of: "http://", with: "https://")
        if url.contains("image.tmdb.org"), !url.contains("/w3840/") {
            let new = url.replacingOccurrences(of: "/original/", with: "/w3840/")
            return new
        }
        
        if url.contains("img.csfd.cz"), !url.contains("?w3840") {
            var new = url + "?w3840"
            if !new.contains("https:"), new.contains("//") {
                new = "https:" + new
            } else if !new.contains("https:"), !new.contains("//") {
                new = "https://" + new
            }
            return new
        }
        return url
    }
    
    ///NOVE spracovavanie pre CSFD
    ///source: https://img.csfd.cz/files/images/film/posters/158/066/158066908_cf9118.jpg
    ///target: https://image.pmgstatic.com/cache/resized/w180/files/images/film/posters/158/066/158066908_cf9118.jpg
    private static func thumbnail(from urlString:String) -> String {
        let url = urlString.replacingOccurrences(of: "http://", with: "https://")
        if url.contains("image.tmdb.org"), !url.contains("/w500/") {
            let new = url.replacingOccurrences(of: "/original/", with: "/w500/")
            return new
        }
        
        if url.contains("img.csfd.cz"), !url.contains("?w600h800") {
            var new = url + "?w600h800"
            if !new.contains("https:"), new.contains("//") {
                new = "https:" + new
            } else if !new.contains("https:"), !new.contains("//") {
                new = "https://" + new
            }
            return new
        }
        
        if url.contains("thetvdb.com"), !url.contains("_t") {
            let newUrl = NSString(string: url)
            let new = newUrl.deletingLastPathComponent + "_t" + newUrl.lastPathComponent
            return new
        }
        
        if url.contains("fanart.tv"), !url.contains("detailpreview") {
            let new = url.replacingOccurrences(of: "assets.fanart.tv", with: "fanart.tv/detailpreview")
            return new
        }
        return url
    }
    
    static func empty<T>() -> T where T: Model {
        let art = Art(poster: "", fanart: "", thumb: "", banner: "", clearart: "", clearlogo: "", landscape: "", icon: "")
        guard let emptyArt = art as?T else {
            fatalError()
        }
        return emptyArt
    }
}

// MARK: - InfoLabels
struct InfoLabels: DataConvertible, Hashable, Model {
    let originaltitle: String?
    let genre: [String]?
    let trailer: String?
    let year: Int?
    let director: [String]?
    let mpaa: String?
    let studio: [String]?
    let premiered, aired, dateadded, mediatype: String?
    let country: [String]?
    let imdbnumber, status: String?
    let season, episode, sortepisode, sortseason: Int?
    let duration: Int?
    let network: String?
    
    func getMediaType() -> MediaType? {
        guard let mediaType = self.mediatype else { return nil }
        return MediaType(rawValue: mediaType)
    }
    
    static func empty<T>() -> T where T: Model {
        let infoLabels = InfoLabels(originaltitle: "",
                                    genre: [],
                                    trailer: "",
                                    year: 1,
                                    director: [],
                                    mpaa: "",
                                    studio: [],
                                    premiered: "",
                                    aired: "",
                                    dateadded: "",
                                    mediatype: "",
                                    country: [],
                                    imdbnumber: "",
                                    status: "",
                                    season: 1,
                                    episode: 0,
                                    sortepisode: 0,
                                    sortseason: 0,
                                    duration: 1,
                                    network: "")
        guard let emptyInfo = infoLabels as?T else {
            fatalError()
        }
        return emptyInfo
    }
}

enum MediaType: String {
    case movie
    case tvshow
    case season
    case episode
}

// MARK: - Services
struct SupportedServices: DataConvertible, Hashable, Model {
    let csfd, imdb, trakt, tvdb: String?
    let slug, tmdb, fanart: String?
    
    var servicesAsQuery:String? {
        get {
            var services:[String:String] = [:]
            if let csfd = csfd {
                services["csfd"] = csfd
            }
            if let imdb = imdb {
                services["imdb"] = imdb
            }
            if let trakt = trakt {
                services["trakt"] = trakt
            }
            if let tvdb = tvdb {
                services["tvdb"] = tvdb
            }
            if let slug = slug {
                services["slug"] = slug
            }
            if let tmdb = tmdb {
                services["tmdb"] = tmdb
            }
            if let fanart = fanart {
                services["fanart"] = fanart
            }
            return self.queryString(params: services)
        }
    }
    
    func queryString(params: [String: String]) -> String? {
        var components = URLComponents()
        components.queryItems = params.map { element in URLQueryItem(name: element.key, value: element.value) }

        return components.url?.absoluteString
    }
    
    static func empty<T>() -> T where T: Model {
        let sService = SupportedServices(csfd: "", imdb: "", trakt: "", tvdb: "", slug: "", tmdb: "", fanart: "")
        guard let emptySService = sService as?T else {
            fatalError()
        }
        return emptySService
    }
}
