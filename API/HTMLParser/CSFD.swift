//
//  CSFD.swift
//  StreamCinema.atv
//
//  Created by SCC on 24/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import SwiftSoup
import Foundation

final class CSFD: NSObject {
    private static let movie: String = "https://www.csfd.cz/film/"
    private var document: Document?
    private var selectPhrase: String = ".related a"
    
    public func getRelated(with csfdID:String, isTvShow: Bool, completion: @escaping (Result<[String], Error>) -> Void) {
        DispatchQueue.global(qos: .background).async {
            guard let url = URL(string: CSFD.movie + csfdID) else { return }
            if isTvShow {
                self.selectPhrase = ".similar a"
            }
            do {
                let html = try String.init(contentsOf: url)
                self.document = try SwiftSoup.parse(html)

                let result = try self.parse()
                DispatchQueue.main.async {
                    completion(.success(result))
                }
            } catch let error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }
    
    private func parse() throws -> [String] {
        guard self.document != nil else { return [] }
        var items:[String] = []
        // firn css selector
        let elements: Elements = try self.document!.select(self.selectPhrase)
        //transform it into a local object (Item)
        for element in elements {
            let html = try element.attr("href")
            let csfdURL = html.replacingOccurrences(of: "/film/", with: "")
            if let csfdID = csfdURL.split(separator: "-").first {
                items.append(String(csfdID))
            }
        }
        return items
    }
}
