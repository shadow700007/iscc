//
//  SCCMovie+Trakt.swift
//  StreamCinema.atv
//
//  Created by SCC on 06/02/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation
import TraktKit

extension SCCMovie {
    mutating public func stop(time:Double, totalTime:Double) {
        let progress = time / totalTime * 100
        let tenPercentOfTotal = totalTime * 0.1
        self.date = Date()
        if (tenPercentOfTotal + time) >= totalTime {
            self.time = 0
        } else {
            self.time = time
        }
        self.progress = Double(progress)
        
        self.saveOrUpdate()
        
        if TraktManager.sharedManager.isSignedIn {
            var movie:SyncId? = nil
            var episode:SyncId? = nil
            
            if self.traktType == .episode,
               let traktID = self.ids?.trakt {
                episode = SyncId(trakt: traktID)
                
            } else if let traktID = self.ids?.trakt {
                movie = SyncId(trakt: traktID)
            }
            
            let scrobble = TraktScrobble(movie: movie, episode: episode, progress: Float(progress) , appVersion: "2", appDate: Date().toDateString)
            _ = try? TraktManager.sharedManager.scrobbleStop(scrobble) { result in
                switch result {
                case .success(object: let object):
                    print(object)
                case .error(error: let error):
                    Log.write("SCCMovie TraktScrobble stop \(String(describing: error?.localizedDescription))")
                }
            }
        }
    }
    
    mutating public func update(time:Double, totalTime:Double) {
        let progress = time / totalTime * 100
        let tenPercentOfTotal = totalTime * 0.1
        self.date = Date()
        if (tenPercentOfTotal + time) >= totalTime {
            self.time = 0
        } else {
            self.time = time
        }
        self.progress = Double(progress)
        self.saveOrUpdate()
        
        if TraktManager.sharedManager.isSignedIn {
            var movie:SyncId? = nil
            var episode:SyncId? = nil
            
            if self.traktType == .episode,
               let traktID = self.ids?.trakt {
                episode = SyncId(trakt: traktID)
                
            } else if let traktID = self.ids?.trakt {
                movie = SyncId(trakt: traktID)
            }
            
            let scrobble = TraktScrobble(movie: movie, episode: episode, progress: Float(progress), appVersion: "2", appDate: Date().toDateString)
            _ = try? TraktManager.sharedManager.scrobbleStart(scrobble, completion: { result in
                switch result {
                case .success(object: let object):
                    print(object)
                case .error(error: let error):
                    Log.write("SCCMovie scrobbleStart \(String(describing: error?.localizedDescription))")
                }
            })
        }
    }
}
