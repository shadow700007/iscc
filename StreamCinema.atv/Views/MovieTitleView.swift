//
//  TitleView.swift
//  StreamCinema.atv
//
//  Created by SCC on 25/10/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit


protocol MovieTitleDelegate: class {
    func movieTitleView(_ view:MovieTitleView)
}

final class MovieTitleView: UIView {
    public var delegate: MovieTitleDelegate?
    
    private let bacgroundImageView: UIImageView = UIImageView()
    private let title: UILabel = UILabel()
    private let genere: UILabel = UILabel()
    private let playButton: UIButton = UIButton()
    private let bgVideoPlayer: BacgroundVideoPlayerView = BacgroundVideoPlayerView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public func setup(imageURL: URL?, trailed: URL?, title: String, genere: String) {
        
    }
    
    private func configureView() {
        self.addSubview(self.bacgroundImageView)
        self.addSubview(self.bgVideoPlayer)
        
        self.bacgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        self.bgVideoPlayer.translatesAutoresizingMaskIntoConstraints = false
        
        self.bacgroundImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.bacgroundImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.bacgroundImageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.bacgroundImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true

        self.bgVideoPlayer.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.bgVideoPlayer.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.bgVideoPlayer.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.bgVideoPlayer.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true

    }
    
}


class BacgroundVideoPlayerView: VideoPlayerView {
    override func customizeUIComponents() {
        super.customizeUIComponents()
        toolBar.isHidden = true
        toolBar.timeSlider.isHidden = true
    }

    override open func player(layer: KSPlayerLayer, state: KSPlayerState) {
        super.player(layer: layer, state: state)
    }

    override func onButtonPressed(type: PlayerButtonType, button: UIButton) {
        super.onButtonPressed(type: type, button: button)
    }
}
