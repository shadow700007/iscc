//
//  MovieCollectionView.swift
//  StreamCinema.atv
//
//  Created by SCC on 06/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

protocol MovieCollectionDelegate {
    func movieCollection(_ collection: MovieCollectionView, didSelect movie: SCCMovie)
    func movieCollection(_ collection: MovieCollectionView, getNext page:Int)
}

struct GengreModel {
    var model:SCCMovieResult?
    var titles:Genere?
}

final class MovieCollectionView: UICollectionView {
    
    public var movieDelegate: MovieCollectionDelegate?
    var model: SCCMovieResult? {
        didSet {
            self.reloadData()
        }
    }
    
    var genreModel: [GengreModel]? {
        didSet {
            self.reloadData()
        }
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.delegate = self
        self.dataSource = self
        self.register(UINib(nibName: "MovieCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        self.register(SectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "SectionHeader")
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.delegate = self
        self.dataSource = self
        self.register(UINib(nibName: "MovieCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        self.register(SectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "SectionHeader")
    }
    
    private func notifyForNextPage() {
        if let current = self.model?.pagination?.page,
            let pageCount = self.model?.pagination?.pageCount,
            current < pageCount  {
            self.movieDelegate?.movieCollection(self, getNext: current + 1)
        }
    }

}

extension MovieCollectionView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    override var numberOfSections: Int {
        if let count = self.genreModel?.count {
            return count
        } else if (self.model?.data.count ?? 0) > 0 {
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let count = self.model?.data.count, count != 0, (count - 1) == indexPath.row {
            self.notifyForNextPage()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.model?.data.count {
            return count
        } else if let model = self.genreModel,
                  model.count > 0,
                  let count = model[section].model?.data.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader,
           let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as? SectionHeader,
           let model = self.genreModel?[indexPath.section] {
            let localizationKey = model.titles?.rawValue ?? ""
            sectionHeader.label.text = String(localizationKey: localizationKey)
            return sectionHeader
        } else {
             return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MovieCell
        
        var data = self.model?.data[indexPath.item]
        data = data ?? self.genreModel?[indexPath.section].model?.data[indexPath.item]
        if let data = data {
            cell.configure(data)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let data = self.model?.data[indexPath.item] {
            self.movieDelegate?.movieCollection(self, didSelect: data)
        } else if let model = self.genreModel?[indexPath.section].model?.data[indexPath.item] {
            self.movieDelegate?.movieCollection(self, didSelect: model)
        }
    }
    
}

final class SectionHeader: UICollectionReusableView {
     var label: UILabel = {
         let label: UILabel = UILabel()
         label.textColor = .white
         label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
         label.sizeToFit()
         return label
     }()

     override init(frame: CGRect) {
         super.init(frame: frame)

         addSubview(label)

         label.translatesAutoresizingMaskIntoConstraints = false
         label.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
         label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
         label.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
