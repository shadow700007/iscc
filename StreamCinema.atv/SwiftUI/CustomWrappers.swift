//
//  CustomWrappers.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 19.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Combine
import SwiftUI


import Combine
import SwiftUI

// MARK: - Input
/// Wrapper creates just the CurrentValueSubject and AnyPublisher for value.
/// The publisher is in `projectedValue` accesible with $.
/// Inspired at : https://www.swiftbysundell.com/articles/connecting-and-merging-combine-publishers-in-swift/
@propertyWrapper
struct Input<Value> {
    var wrappedValue: Value {
        get { subject.value }
        set { subject.send(newValue) }
    }

    let projectedValue: AnyPublisher<Value, Never>

    private let subject: CurrentValueSubject<Value, Never>

    init(wrappedValue: Value) {
        self.subject = CurrentValueSubject(wrappedValue)
        self.projectedValue =
            subject
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}

