//
//  TraktViewModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 24/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import TraktKit
import Combine

final class TraktViewModel: NSObject {

    var allPages:TraktPagination
    var data: TraktItemsData
    
    var onReloadData: ((_ forCell:IndexPath) -> Void)?

    let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelabels: Set<AnyCancellable> = Set()

    private let appData: AppData

    init(appData: AppData)
    {
        self.appData = appData
        self.allPages = TraktPagination()
        self.data = TraktItemsData()
        super.init()
        self.loadData()
    }
    
    private func loadData() {
        if appData.traktManager?.isSignedIn == true {
            self.getWatchList(.Movies)
            self.getWatchList(.Shows)
            self.getHistory(type: .Movies)
            self.getHistory(type: .Shows)
            self.getMeLists()
            self.getFollowers()
        }
    }
}

//MARK: - table methods
extension TraktViewModel {
    func numberOfRows() -> Int {
        return self.data.count
    }
    
    func cellForRow(at indexPath:IndexPath, for tableView: UITableView, delegate: TraktViewController) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesCollectionCell") as? MoviesCollectionCell,
              self.numberOfRows() > indexPath.row
        else { return UITableViewCell() }

        if let data = self.data.data(forCellAt: indexPath) {
            cell.set(data.items, name:data.name)
        }
        cell.delegate = delegate
        cell.indexPath = indexPath
        return cell
    }
    
}

extension TraktViewModel {
    public func fetchData(for indexPath:IndexPath) {
        
    }
    
    func getWatchList(_ type:TraktKit.WatchedType = .Movies) {
        var pageToLoad = self.allPages.nextPageWatchList.pagination
        if type == .Shows {
            pageToLoad = self.allPages.nextPageTvShowWatchList.pagination
        }
        TraktManager.sharedManager.getWatchlist(watchType: type,
                                                pagination: pageToLoad) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(objects: let objects, currentPage: let currentPage, limit: let limit):
                var items = objects.map { item -> SCCMovie in
                    return SCCMovie(listItem: item)
                }
                items = items.filterDuplicates(includeElement: { $0 == $1 })
                let nextPage = TraktKit.Pagination(page: currentPage + 1, limit: pageToLoad.limit)
                var title:String = ""
                if type == .Movies {
                    self.allPages.nextPageWatchList.pagination = nextPage
                    self.allPages.nextPageWatchList.totalPages = limit
                    title = TraktCellType.watchList.title
                } else  if type == .Shows {
                    self.allPages.nextPageTvShowWatchList.pagination = nextPage
                    self.allPages.nextPageTvShowWatchList.totalPages = limit
                    title = TraktCellType.tvShowWatchList.title
                }
                self.resolveTrakt(items: items, name: title)
            case .error(error: let maybeError):
                guard let error = maybeError else { return }
                self.errorSubject.send(error)
            }
        }
    }
    
    func getHistory(type: TraktKit.WatchedType = .Movies) {
        var page = self.allPages.nextPageTraktHistoryMovieList.pagination
        if type == .Shows {
            page = self.allPages.nextPageTraktHistoryShowsList.pagination
        }
        TraktManager.sharedManager.getUserWatchedHistory(type: type,startAt: nil, endAt: nil, extended: [.Full], pagination: page) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(objects: let objects, currentPage: let currentPage, limit: let limit):
                var items = objects.map { item -> SCCMovie in
                    return SCCMovie(traktItem: item, type: type)
                }
                items = items.filterDuplicates(includeElement: { $0 == $1 })
                let nextPage = TraktKit.Pagination(page: currentPage + 1, limit: page.limit)
                var name:String = "No Name"
                if type == .Movies {
                    self.allPages.nextPageTraktHistoryMovieList.pagination = nextPage
                    self.allPages.nextPageTraktHistoryMovieList.totalPages = limit
                    name = TraktCellType.movieHistory.title
                }
                if type == .Shows {
                    self.allPages.nextPageTraktHistoryShowsList.pagination = nextPage
                    self.allPages.nextPageTraktHistoryShowsList.totalPages = limit
                    name = TraktCellType.showHistory.title
                }
                self.resolveTrakt(items: items, name: name)
                
            case .error(error: let maybeError):
                guard let error = maybeError else { return }
                self.errorSubject.send(error)
            }
        }
    }
    
    func resolveTrakt(items:[SCCMovie], name:String) {
        self.fetchDataFromSCC(traktData: items)
            .sink { [weak self] result in
                let newIdem = TraktItem(name: name, items: result)
                let index = self?.data.append(item:newIdem)
                self?.onReloadData?(IndexPath(row: index!, section: 0))
            }
            .store(in: &cancelabels)
    }
    
    private func getFollowers() {
        TraktManager.sharedManager.getUserFollowing { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(objects: let objects):
                let users = objects.map { follower -> TraktKit.User in
                    return follower.user
                }
                self.getLists(of: users)
                break
            case .error(error: let maybeError):
                guard let error = maybeError else { return }
                self.errorSubject.send(error)
            }
        }
    }
    
    private func getLists(of friends:[TraktKit.User]) {
        for friend in friends {
            guard let userName = friend.username else { return }
            TraktManager.sharedManager.getCustomLists(username: userName) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(objects: let objects):
                    for object in objects {
                        self.getDataFromList(object.ids, name: userName + " - " + object.name, username: userName)
                    }
                case .error(error: let maybeError):
                    guard let error = maybeError else { return }
                    self.errorSubject.send(error)
                }
            }
        }
    }
    
    private func getMeLists() {
        TraktManager.sharedManager.getCustomLists { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(objects: let objects):
                for object in objects {
                    self.getDataFromList(object.ids, name: object.name)
                }
            case .error(error: let maybeError):
                guard let error = maybeError else { return }
                self.errorSubject.send(error)
            }
        }
    }
    
    private func getDataFromList(_ ids:ListId, name:String, username:String = "me") {
        TraktManager.sharedManager.getItemsForCustomList(username: username, listID: ids.trakt) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(objects: let objects):
                let items = objects.map { item -> SCCMovie in
                    return SCCMovie(listItem: item)
                }
                if items.count > 0 {
                    self.resolveTrakt(items: items, name:name)
                }
            case .error(error: _):
                break
            }
        }
    }

    private func fetchDataFromSCC(traktData: [SCCMovie]) -> AnyPublisher<[SCCMovie], Never> {
       let traktIDs = self.map(data: traktData)
        return appData.scService
            .getMovies(traktID: traktIDs, page: 1)
            .assignError(to: errorSubject)
            .map { fetchedResult -> [SCCMovie] in
                return self.mearge(traktData: traktData, sccData: fetchedResult.data)
            }
            .eraseToAnyPublisher()
    }
    
    private func mearge(traktData:[SCCMovie], sccData:[SCCMovie]) -> [SCCMovie] {
        if traktData.count == 0 {
            return sccData
        }
        var meargeData = traktData
        for i in 0...meargeData.count-1 {
            if let sccItem = sccData.first(where: { $0 == meargeData[i] }) {
                meargeData[i].mearge(with: sccItem)
            }
        }
        return meargeData
    }
    
    private func map(data: [SCCMovie]) -> [String] {
        return data.map { movie -> String in
            switch movie.traktType {
            case .movie,
                 .tvshow:
                return "\(movie.traktType.rawValue):\(movie.ids?.trakt ?? 0)"
            case .episode:
                return "\(movie.traktType.rawValue):\(movie.ids?.trakt ?? 0)"
            case .season:
                return "\(movie.traktType.rawValue):\(movie.ids?.trakt ?? 0)"
            }
        }
    }
}

extension TraktViewModel {

    enum TraktViewModelError: Error {
        case fetchingTVShowDataButItIsNotTVSHOW
        case missingTraktID
    }

    func sccTVShowFromEpisodeData(for item: SCCMovie) -> AnyPublisher<SCCMovie?, Never> {
        guard item.traktType == .episode else {
            return Just(nil).eraseToAnyPublisher()
        }
        let traktID = item.rootIDs?.trakt
        let tmdbID = item.rootIDs?.tmdb
        let imdbID = item.rootIDs?.imdb

        return sccDataFromTrakt(traktID: traktID, for: item)
            .flatMap { traktData -> AnyPublisher<SCCMovie?, Never> in
                if let data = traktData {
                    return Just(data).eraseToAnyPublisher()
                } else {
                    return self.sccDataFromTmdb(tmdbID: tmdbID)
                }
            }.flatMap { maybeTraktOrTmdbData -> AnyPublisher<SCCMovie?, Never>  in
                if let data = maybeTraktOrTmdbData {
                    return Just(data).eraseToAnyPublisher()
                } else {
                    return self.sccDataFromImdb(imdbID: imdbID)
                }
            }
            .eraseToAnyPublisher()
    }

    func sccData(for item: SCCMovie) -> AnyPublisher<SCCMovie?, Never> {
        let traktID = item.traktType == .episode ? item.episodeIDs?.trakt : item.rootIDs?.trakt
       let tmdbID = item.traktType == .episode ? item.episodeIDs?.tmdb : item.rootIDs?.tmdb
       let imdbID = item.traktType == .episode ? item.episodeIDs?.imdb : item.rootIDs?.imdb

       return  sccDataFromTrakt(traktID: traktID, for: item)
            .flatMap { maybeData -> AnyPublisher<SCCMovie?, Never> in
                if let data = maybeData {
                    return Just(data).eraseToAnyPublisher()
                } else {
                    return self.sccDataFromTmdb(tmdbID: tmdbID)
                }
            }.flatMap { (maybeTraktOrTmdbData) -> AnyPublisher<SCCMovie?, Never> in
                if let data = maybeTraktOrTmdbData {
                    return Just(data).eraseToAnyPublisher()
                } else {
                    return self.sccDataFromImdb(imdbID: imdbID).eraseToAnyPublisher()
                }
            }
            .eraseToAnyPublisher()
    }

    func sccDataFromTrakt(traktID: Int?, for item: SCCMovie) -> AnyPublisher<SCCMovie?, Never> {
        guard let traktID = traktID else {
            return Just(nil).eraseToAnyPublisher()
        }
        let traktString:SCCType = item.traktType == .episode ? .tvshow : .movie
        return appData.scService
            .getMovies(traktID: ["\(traktString.rawValue):\(traktID)"], page: 1)
            .assignError(to: errorSubject)
            .map { $0.data.first }
            .eraseToAnyPublisher()
    }

    func sccDataFromImdb(imdbID: String?) -> AnyPublisher<SCCMovie?, Never> {
        guard let imdbID = imdbID else { return Just(nil).eraseToAnyPublisher() }
        return appData.scService.getMovie(imdbID: imdbID)
            .assignError(to: errorSubject)
            .map { $0.data.first }
            .eraseToAnyPublisher()
    }


    func sccDataFromTmdb(tmdbID: Int?) -> AnyPublisher<SCCMovie?, Never> {
        guard let tmdbID = tmdbID else { return Just(nil).eraseToAnyPublisher() }
        return appData.scService
            .getMovie(tmdb: tmdbID)
            .assignError(to: errorSubject)
            .map { $0.data.first }
            .eraseToAnyPublisher()
    }
}

struct Page  {
    var pagination: TraktKit.Pagination
    var totalPages: Int
}

struct TraktItem {
    var name: String
    var items: [SCCMovie]
    
    init(name: String, items: [SCCMovie]) {
        self.items = items
        self.name = name.lowercased()
        self.name.capitalizeFirstLetter()
    }
}

struct TraktItemsData {
    var items: [TraktItem] = []
    var type: TraktCellType = .unowned
    var pagination: TraktKit.Pagination = TraktKit.Pagination(page: 1, limit: 10)
    var totalPages: Int = 0
    
    var count:Int {
        return self.items.count
    }
    
    func data(forCellAt indexPath:IndexPath) -> TraktItem? {
        if let item = self.items[safe: indexPath.row] {
            return item
        }
        return nil
    }
    
    @discardableResult
    mutating func append(item: TraktItem) -> Int {
        let foundItem = self.items.firstIndex(where: { $0.name.lowercased() == item.name.lowercased() })
        if let foundItem = foundItem,
           var oldItems = self.items[safe: foundItem] {
            oldItems.items.append(contentsOf: item.items)
            oldItems.items = oldItems.items.filterDuplicates(includeElement: { $0 == $1 })
            self.items[foundItem] = oldItems
            return foundItem
        } else {
            self.items.append(item)
            self.items.sort(by: { $0.name < $1.name })
            return self.items.firstIndex(where: { $0.name == item.name })!
        }
    }
}

struct TraktPagination {
    var nextPageWatchList:Page
    var nextPageTvShowWatchList:Page
    var nextPageTraktHistoryMovieList:Page
    var nextPageTraktHistoryShowsList:Page
    
    init() {
        self.nextPageWatchList = Page(pagination: TraktKit.Pagination(page: 1, limit: 10), totalPages: 0)
        self.nextPageTvShowWatchList = Page(pagination: TraktKit.Pagination(page: 1, limit: 10), totalPages: 0)
        self.nextPageTraktHistoryMovieList = Page(pagination: TraktKit.Pagination(page: 1, limit: 10), totalPages: 0)
        self.nextPageTraktHistoryShowsList = Page(pagination: TraktKit.Pagination(page: 1, limit: 10), totalPages: 0)
    }
}
