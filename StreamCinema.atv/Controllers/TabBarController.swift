//
//  TabBarController.swift
//  StreamCinema.atv
//
//  Created by SCC on 03/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine

final class TabBarController: UITabBarController {

    static func create(appData: AppData) -> TabBarController {
        let storyboard = UIStoryboard.main
        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        vc.appData = appData
        return vc
    }

    private var appData: AppData!
    private var completition: ((UIAlertAction) -> Void)?
    private var coordinator: MovieCoordinator?

    private let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let image = UIImage(named: "backgroundImage") {
            self.view.backgroundColor = UIColor(patternImage: image)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkCredentials()
        self.testSpeed()
    }
    
    public func presentDetailFormOpenIn(type: FilterType, movieID: String, startPlaying: Bool) {
        guard self.isViewLoaded == true,
            !(self.presentedViewController?.isEqual(LoginViewController.self) ?? false) else { return }

        appData.scService
            .getMovieScc(by: movieID, type: type)
            .assignError(to: errorSubject)
            .sink { value in
                if let mediaType = value.infoLabels?.getMediaType() {
                    fatalError()
//                let data = InfoData(index: nil, type: nil, id: movieID, score: nil, source: value, tvInfo: nil)
//                    self.presentDetialVD(type: mediaType, movieID: movieID, infoData: data)
                }
            }.store(in: &cancelables)
    }

    private func testSpeed() {
        appData.speed
            .testSpeed()
            .assignError(to: errorSubject)
            .sink { (bitCount, speed) in
                Log.write("Speed result - bitCount: \(bitCount), speed: \(speed)")
            }.store(in: &cancelables)
    }

    private func setupSubsriptions() {
        errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                guard let otherError = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherError)
            }.store(in: &cancelables)
    }
    
    private func checkCredentials() {
        if LoginManager.tokenHash == nil, self.isViewLoaded == true {
//            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OnboardingViewController")
//            self.present(vc, animated: true, completion: nil)
        }
    }
    
    private func presentDetialVD(type:MediaType, movieID:String, infoData: SCCMovie) {
        guard let rootViewController = self.navigationController else { return }
        self.coordinator = MovieCoordinator(appData: appData, rootViewController: rootViewController)
        coordinator?.presentMovieAndShowViewController(movieData: infoData)
    }
    
    override var selectedIndex: Int {
        didSet {
//            self.checkCredentials()
        }
    }
}
