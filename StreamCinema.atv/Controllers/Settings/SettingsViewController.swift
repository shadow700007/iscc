//
//  SettingsViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 02/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import TraktKit
import Combine


final class SettingsViewController: UITableViewController {

    static func create(viewModel: SettingsViewModel) -> SettingsViewController {
        let setting = SettingsViewController(style: .grouped)
        setting.viewModel = viewModel
        return setting
    }

    // MAKR: - Nested data types
    struct Credentials {
        var name: String
        var pass: String
    }
    
    struct PassCode {
        var pass: String
        var reTypePass: String
    }

    var type: FilterType?
    
    private var credentials:Credentials = Credentials (name: "", pass: "") {
        didSet {
            viewModel.chceckCredentials(credentials: credentials)
        }
    }

    private var viewModel: SettingsViewModel!
    private var workInProggress:UILabel = UILabel()
    private var alert: UIAlertController?
    private var progressView: UIProgressView?
    private var appPassCode:PassCode?
    public var currentSettings: SettingsMenuItem = .webshare
    private var cancelables: Set<AnyCancellable> = Set()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
        setupMenuButtonToForceFocusOnTabBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.fetchWSUserData()
        viewModel.fetchTraktData()
    }
    
    private func setupView() {
        self.tableView.register(SettingsInputTextCell.self, forCellReuseIdentifier: "SettingsInputTextCell")
        self.tableView.register(InfoCell.self, forCellReuseIdentifier: "InfoCell")
        self.tableView.register(ButtonCell.self, forCellReuseIdentifier: "ButtonCell")
    }

    private func setupViewModel() {
        viewModel.onReloadData = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }

        viewModel.errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (erorr) in
                guard let otherErr = self?.handleCommonError(erorr) else { return }
                self?.presentErrorAlert(error: otherErr)
            }.store(in: &cancelables)
    }
    
    
    private func showWorkInProgressIfNeeded(count: Int) {
        if count > 0 {
            self.tableView.backgroundView = nil
        } else {
            self.tableView.backgroundView = self.workInProggress
            self.workInProggress.text = String(localized: .setting_in_progress)
            self.workInProggress.textAlignment = .center
        }
    }
    
    private func clearWatchedHistoryAppData() {
        let alert = UIAlertController(title: String(localized: .setting_delete_data_title), message: String(localized: .setting_delete_data_message), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: String(localized: .buttonDelete), style: .destructive, handler: { _ in
            WatchedWrapper.deleteAllData(for: .scc)
        }))
        
        alert.addAction(UIAlertAction(title: String(localized: .buttonCancel), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setFontSize() {
        let alert = UIAlertController(title: String(localized: .subtitles_font_size), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: String(localized: .subtitles_font_size_small), style: .default, handler: { _ in
            SubtitlesSettings.subtitlesSize = .small
            self.tableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: String(localized: .subtitles_font_size_normal), style: .default, handler: { _ in
            SubtitlesSettings.subtitlesSize = .normal
            self.tableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: String(localized: .subtitles_font_size_large), style: .default, handler: { _ in
            SubtitlesSettings.subtitlesSize = .large
            self.tableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: String(localized: .subtitles_font_size_large), style: .default, handler: { _ in
            SubtitlesSettings.subtitlesSize = .larger
            self.tableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: String(localized: .buttonCancel), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setFontColor() {
        let alert = UIAlertController(title: String(localized: .subtitles_font_color), message: nil, preferredStyle: .alert)
        
        for color in SubtitleColor.allCases {
            alert.addAction(UIAlertAction(title: color.colorName, style: .default, handler: { _ in
                SubtitlesSettings.subtitlesColor = color
                self.tableView.reloadData()
            }))
        }
        
        alert.addAction(UIAlertAction(title: String(localized: .buttonCancel), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func clearAppData() {
        let alert = UIAlertController(title: String(localized: .setting_delete_data_title), message: String(localized: .setting_delete_data_message), preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: String(localized: .buttonDelete), style: .destructive, handler: { [weak self] _ in
            guard let `self` = self else { return }
            self.viewModel
                .logout()
                .sink { [weak self] _ in
                    self?.tabBarController?.selectedIndex = 0
                }.store(in: &self.cancelables)
        }))
        
        alert.addAction(UIAlertAction(title: String(localized: .buttonCancel), style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: nil, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showNeedCredentials(message: String? = String(localized: .to_watch_the_content_you_need_vip)) {
        self.alert = UIAlertController(title: String(localized: .missing_credentials),
                                       message: message ,
                                       preferredStyle: .alert)
        self.alert?.addTextField(configurationHandler: { textField in
            textField.placeholder = String(localized: .username)
        })
        self.alert?.addTextField(configurationHandler: { textField in
            textField.isSecureTextEntry = true
            textField.placeholder = String(localized: .password)
        })
        
        let action = UIAlertAction(title: String(localized: .button_continue), style: .default) { [weak self] _ in
            guard let self = self else { return }
            let name = self.alert?.textFields?[0].text
            let pass = self.alert?.textFields?[1].text
            self.login(name: name, pass: pass)
        }
        self.alert?.addAction(UIAlertAction(title: nil, style: .cancel, handler: nil))
        self.alert?.addAction(action)
        self.present(alert!, animated: true, completion: nil)
    }
    
    private func login(name: String? = nil, pass: String? = nil) {
        viewModel
            .connect(name: name, pass: pass)
            .sink { [weak self] completion in
                guard case .failure(let error) = completion else { return }
                if case .unknownUserLoginCredentials = error as? LoginManager.LoginManagerError {
                    self?.showNeedCredentials()
                } else {
                    self?.showNeedCredentials(message: String(localized: .bad_credentials))
                }
            } receiveValue: { [weak self] _ in
                guard let `self` = self else { return }
                if let alert = self.alert {
                    alert.dismiss(animated: false, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                    return
                }
                self.dismiss(animated: true, completion: nil)
            }.store(in: &cancelables)
    }
    
    private func disableErotic() {
        let alert = UIAlertController(title: String(localized: .settingsErocitcTitle),
                                      message: String(localized: .settingsErocitcMessageDisable),
                                      preferredStyle: .alert)

        if CurrentAppSettings.isPassCodeEnabled {
            alert.message = String(localized: .settingsErocitcMessageEnable)
            
            alert.addTextField(configurationHandler: { textField in
                textField.isSecureTextEntry = true
                textField.placeholder = String(localized: .password)
            })
        } else {
            alert.addTextField(configurationHandler: { textField in
                textField.isSecureTextEntry = true
                textField.placeholder = String(localized: .password)
            })
            alert.addTextField(configurationHandler: { textField in
                textField.isSecureTextEntry = true
                textField.placeholder = String(localized: .reTypePassword)
            })
        }
        
        alert.addAction(UIAlertAction(title: String(localized: .buttonOK),
                                      style: .default,
                                      handler: { [weak self] action in
                                        if let pass = alert.textFields?[safe:0]?.text,
                                           let reType = alert.textFields?[safe:1]?.text,
                                           reType == pass {
                                            CurrentAppSettings.passCode = pass
                                        } else if let pass = alert.textFields?[safe:0]?.text,
                                                  alert.textFields?[safe:1] == nil,
                                                  pass == CurrentAppSettings.passCode {
                                            CurrentAppSettings.passCode = nil
                                        } else {
                                            self?.disableErotic()
                                        }
                                      }))
        alert.addAction(UIAlertAction(title: String(localized: .buttonCancel), style: .cancel, handler: nil))
        
        self.appPassCode = PassCode(pass: "", reTypePass: "")
        self.present(alert, animated: true, completion: nil)
    }
}

extension SettingsViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        let sectionsCount = self.viewModel.getNumberOfSections(for: self.currentSettings)
        self.showWorkInProgressIfNeeded(count: sectionsCount)
        return sectionsCount
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionsForMenu = currentSettings.sections
        guard let section = sectionsForMenu[safe: section] else { return 0 }
        return self.viewModel.getNumberOfCells(for: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionsForMenu = currentSettings.sections
        guard let section = sectionsForMenu[safe: indexPath.section],
            let cell = self.viewModel.getCell(tableView, of: section, forRowAt: indexPath) else { return UITableViewCell()
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        let sectionsForMenu = currentSettings.sections
        if let section = sectionsForMenu[safe: indexPath.section] {
            return viewModel.isFocusableCell(tableView, of: section, forRowAt: indexPath)
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? SettingsCell,
            let type = cell.type else { return }
        switch type {
        case .userName,
             .password,
             .appVersion:
            break
        case .vipExpiration:
            break
        case .changeButton:
            self.showNeedCredentials()
        case .resetButton:
            self.clearAppData()
        case .wsSpeedTestResult:
            break //
        case .wsSpeedTestButton:
            viewModel.testSpeed()
        case .resetWatchedhistory:
            self.clearWatchedHistoryAppData()
        case .loginTraktButton:
            self.connectToTakt()
        case .wsLogout:
            self.clearAppData()
        case .traktUserName:
            break
        case .traktUserIsVIP:
            break
        case .openSubtitlesUserName:
            break
        case .loginOpenSubtitles:
            self.loginOS()
        case .subtitlesFontSize:
            self.setFontSize()
        case .subtitlesFontColor:
            self.setFontColor()
        case .disableErotic:
            self.disableErotic()
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let section = SettingsSections(rawValue: section) {
            return self.viewModel.getSectionTitle(for: section)
        }
        return nil
    }
}

extension SettingsViewController: SettingsInputTextCellDelegate {
    func settingsInput(_ cell: SettingsInputTextCell, type: CellTypes, set value: String) {
        switch type {
        case .password:
            self.credentials.pass = value
        case .userName:
            self.credentials.name = value
        default:
            break
        }
    }
}

extension SettingsViewController: MenuViewDelegate {
    
    func menuView(_ menuView: MenuViewController, settingsDid select: SettingsMenuItem) {
        self.currentSettings = select
        self.tableView.reloadData()
        
    }
    
    func menuView(_ menuView: MenuViewController, did select: MenuItem) {    }
}

//MARK: - openSubbtirtles
extension SettingsViewController {
    private func loginOS() {
        alert = UIAlertController(title: String(localized: .missing_credentials),
                                       message: String(localized: .to_watch_the_content_you_need_vip) ,
                                       preferredStyle: .alert)
        alert?.addTextField(configurationHandler: { textField in
            textField.placeholder = String(localized: .username)
        })
        alert?.addTextField(configurationHandler: { textField in
            textField.isSecureTextEntry = true
            textField.placeholder = String(localized: .password)
        })
        
        let action = UIAlertAction(title: String(localized: .button_continue), style: .default) { [weak self] _ in
            guard let self = self else { return }
            let name = self.alert?.textFields?[0].text
            let pass = self.alert?.textFields?[1].text
            self.viewModel.loginToOS(name: name, pass: pass)
        }
        
        alert?.addAction(action)
        self.present(alert!, animated: true, completion: nil)
    }
}

//MARK: - trakt kit
extension SettingsViewController: TraktQRViewDelegate {
    func traktQRView(_ traktViewController: TraktQRViewController, isLogin success: Bool) {
        if success {
            viewModel.fetchTraktData()
        }
    }
    
    func connectToTakt() {
        TraktManager.sharedManager.getAppCode { [weak self] deviceData in
            guard let self = self else { return }
            if let deviceCode = deviceData {
                self.showQRCode(data: deviceCode)
            }
        }
    }
    
    func showQRCode(data: DeviceCode) {
        DispatchQueue.main.async {
            let traktVC = TraktQRViewController()
            traktVC.traktDelegate = self
            traktVC.set(data: data)
            self.present(traktVC, animated: true,completion: nil)
        }
    }
}
