//
//  ActorMoviesViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine

final class ActorMoviesViewController: UIViewController {

    static func create(appData: AppData) -> ActorMoviesViewController {
        let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "ActorMoviesViewController") as! ActorMoviesViewController
        vc.appData = appData
        return vc
    }
    
    @IBOutlet weak var actorImageView: UIImageView!
    @IBOutlet weak var actorNameLabel: UILabel!
    @IBOutlet weak var movieCollection: MovieCollectionView!
    @IBOutlet weak var activityIndikator: UIActivityIndicatorView!
    private var coordinator: MovieCoordinator?
    private var cast:Cast?
    private var model:SCCMovieResult?
    private var movieTitle:String?

    private var appData: AppData!
    private var cancelables: Set<AnyCancellable> = Set()
    private let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()

    var onPresentMovieDetailScreen: ((SCCMovie) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.movieCollection.movieDelegate = self
        if let cast = self.cast {
            self.actorNameLabel.text = cast.name
            if let thumbnail = cast.thumbnail,
                let actorImgUrl = URL(string: thumbnail) {
                self.actorImageView.setCashedImage(url: actorImgUrl, type: .emptyLoading)
            }
            self.fetchData()
        } else if let model = self.model,
                  let title = self.movieTitle {
            self.movieCollection.model = model
            self.actorNameLabel.text = title
        }
    }
    
    public func prepareData(with cast:Cast) {
        self.cast = cast
    }
    
    public func prepareData(with model:SCCMovieResult, releated title:String) {
        self.movieTitle = title
        self.model = model
    }
    
    private func fetchData(for page:Int = 1) {
        guard let castName = cast?.name else {
            return
        }
        self.activityIndikator.startAnimating()
        let filter = CustomFilterModel(with: castName)
        appData.scService
            .customSearch(wtih: filter)
            .sink { [weak self] completion in
                guard case .failure(let err) = completion else { return }
                self?.activityIndikator.stopAnimating()
                self?.errorSubject.send(err)
            } receiveValue: { [weak self] (filter) in
                self?.activityIndikator.stopAnimating()
                self?.movieCollection.model = filter
            }.store(in: &cancelables)
    }

}

extension ActorMoviesViewController: MovieCollectionDelegate {
    func movieCollection(_ collection: MovieCollectionView, didSelect movie: SCCMovie) {
        onPresentMovieDetailScreen?(movie)
    }
    
    func movieCollection(_ collection: MovieCollectionView, getNext page: Int) {
        if self.cast != nil {
            self.fetchData(for: page)
        }
    }
}
