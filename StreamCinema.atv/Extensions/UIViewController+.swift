//
//  UIViewController+.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 19.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

extension UIViewController {
    var firstNavigationController: UINavigationController? {
        if let navigationController = self.navigationController
            ?? (self as? UINavigationController)
        {
            return navigationController
        }

        return
            children
            .first(where: { $0.firstNavigationController != nil })?
            .firstNavigationController
    }

    // This is temporary solution for iOS < 14.0 and can be removed when stop support iOS 13.X
    var firstTabBarController: UITabBarController? {
        if let tabBarController = self.tabBarController
            ?? (self as? UITabBarController)
        {
            return tabBarController
        }

        return
            children
            .first(where: { $0.firstTabBarController != nil })?
            .firstTabBarController
    }

    func setupMenuButtonToForceFocusOnTabBar() {
        if IsccOptions.runNewTabBar == false {
            return
        }
        let sentNotification = {
            NotificationCenter.default.post(
                name: .focusTabBar,
                object: ()
            )
        }

        let menuPressRecognizer = UITapGestureRecognizer()
        menuPressRecognizer.onTap = sentNotification
        menuPressRecognizer.allowedPressTypes = [NSNumber(value: UIPress.PressType.menu.rawValue)]
        self.view.addGestureRecognizer(menuPressRecognizer)
    }
}

extension UISplitViewController {


    open override func viewDidLoad() {
        super.viewDidLoad()
        setupMenuButtonToForceFocusOnTabBar()
    }

}
