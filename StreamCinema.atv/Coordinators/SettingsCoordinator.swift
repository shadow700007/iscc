//
//  SettingsCoordinator.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 25.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

final class SettingsCoordinator: Coordinator {

    let rootViewController: UINavigationController
    private let appData: AppData

    private let settings: SettingsViewController = SettingsViewController(style: .grouped)
    private let menu: MenuViewController = MenuViewController()

    init(appData: AppData, rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
        self.appData = appData
    }

    func start() { }


    func createVC(for tabBar: TabBarItem) -> UIViewController {
        let vc = createSettingScreen()
        vc.tabBarItem.title = tabBar.description
        return vc
    }

    func presentSettingScreen() {
        let vc = createSettingScreen()
        rootViewController.pushViewController(vc, animated: true)
    }

    private func createSettingScreen() -> UISplitViewController {
        let viewModel = SettingsViewModel(appData: appData)
        let vc = SettingsViewController.create(viewModel: viewModel)
        let menu: MenuViewController = MenuViewController()
        menu.endpoit = TabBarItem.settings
        menu.menuDelegate = vc
        let split = UISplitViewController()
        split.viewControllers = [menu, vc]
        split.title = TabBarItem.settings.description
        split.preferredPrimaryColumnWidthFraction = 2/7
        return split
    }

}
