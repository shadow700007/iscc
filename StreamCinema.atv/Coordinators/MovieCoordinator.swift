//
//  TVShowCoordinator.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class MovieCoordinator: Coordinator {

    let rootViewController: UINavigationController
    private let appData: AppData

    // Child coordinators
    private var vlcCoordinator: VLCCoordinator?


    init(appData: AppData, rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
        self.appData = appData
    }

    func start() {
        // Some data preperations
    }

    func createVC(for item: TabBarItem) -> UIViewController {
        let vc = createMovieListScreen(endpoint: item)
        vc.tabBarItem.title = item.description
        return vc
    }

    func presentMovieList(for item: TabBarItem) {
        let vc = createMovieListScreen(endpoint: item)
        rootViewController.pushViewController(vc, animated: true)
    }

    func presentMovieAndShowViewController(movieData: SCCMovie) {
        let viewModel = MovieAndShowViewModel(movieData: movieData, appData: appData)
        let vc = MovieAndShowViewControllera.create(viewModel: viewModel)
        vc.delegate = self
        rootViewController.pushViewController(vc, animated: true)
    }

    private func presentActorsMoviesViewController(cast: Cast) {
        let vc = ActorMoviesViewController.create(appData: appData)
        vc.onPresentMovieDetailScreen = { [weak self] movie in
            self?.presentMovieAndShowViewController(movieData: movie)
        }
        vc.prepareData(with: cast)
        rootViewController.pushViewController(vc, animated: true)
    }

    private func presentVLCPlayer(modelData: MovieModel, isContinue: Bool = false) {

        let vlcCoordinator = VLCCoordinator(appData: appData, rootViewController: rootViewController)
        self.vlcCoordinator = vlcCoordinator

        if modelData.type == .movie {
            vlcCoordinator.presentVLCPlayer(modelData: modelData, startAt: 0, isContinue: isContinue)
        } else {
            let currentSelected = modelData.selected
            guard currentSelected.episode <= modelData.episodes.count else { return }
            vlcCoordinator.presentVLCPlayer(modelData: modelData, startAt: currentSelected.episode, isContinue: isContinue)
        }
        vlcCoordinator.start()
    }

    private func createMovieListScreen(endpoint: TabBarItem) -> UIViewController {
        let vc = MovieListViewController.create(appData: appData)
        vc.onPresentMovieDetailScreen = { [weak self] model in
            self?.presentMovieAndShowViewController(movieData: model)
        }
        let menu: MenuViewController = MenuViewController()
        menu.endpoit = endpoint
        menu.menuDelegate = vc
        let split = UISplitViewController()
        split.viewControllers = [menu, vc]
        split.preferredPrimaryColumnWidthFraction = 2/7
        return split
    }

}


extension MovieCoordinator: MovieViewControllerDelegate {

    func presentCastDetailScreen(for cast: Cast) {
        self.presentActorsMoviesViewController(cast: cast)
    }

    func presentMovieDetailScreen(for relatedMovie: SCCMovie) {
        self.presentMovieAndShowViewController(movieData: relatedMovie)
    }

    func presentVideoPlayer(model: MovieModel, isContinue: Bool) {
        self.presentVLCPlayer(modelData: model, isContinue: isContinue)
    }
}


