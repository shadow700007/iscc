//
//  TraktCoordinator.swift
//  StreamCinema.atv
//
//  Created by iSCC on 16/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation
import TraktKit
import UIKit
import TMDBKit
import FanArtKit


final class TraktCoordinator: Coordinator {
    
    let rootViewController: UINavigationController
    private let appData: AppData

    var traktViewController: TraktViewController?
    private var movieCoordinator: MovieCoordinator?

    init(appData: AppData, rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
        self.appData = appData
    }

    func start() {
        // Some data preparations
    }

    func createVC(for tabBar: TabBarItem) -> UIViewController {
        let vc = createTraktListScreen()
        vc.tabBarItem.title = tabBar.description
        return vc
    }

    func presentTraktList() {
        let vc = createTraktListScreen()
        rootViewController.pushViewController(vc, animated: true)
    }


    private func createTraktListScreen() -> UIViewController {
        let viewModel =  TraktViewModel(appData: appData)
        let vc = TraktViewController.create(viewModel: viewModel)
        vc.movieDelegate = self
        self.traktViewController = vc
        return vc
    }
}

extension TraktCoordinator: TraktViewControllerDelegate {

    func traktView(_ controller: TraktViewController, didSelect item: SCCMovie) {
        presentMovieDetail(data: item)
    }

    private func presentMovieDetail(data: SCCMovie) {
        self.movieCoordinator = MovieCoordinator(appData: appData, rootViewController: rootViewController)
        movieCoordinator?.start()
        movieCoordinator?.presentMovieAndShowViewController(movieData: data)
    }
}
