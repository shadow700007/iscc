//
//  IsccOptions.swift
//  StreamCinema.atv
//
//  Created by SCC on 06/02/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation

enum PlayerType {
    case vlc
}

final class IsccOptions {
    static var runNewTabBar: Bool  = false
    
    static var userdPlayer: PlayerType  = .vlc
}
