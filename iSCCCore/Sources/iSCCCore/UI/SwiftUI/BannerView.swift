//
//  SwiftUIView.swift
//  
//
//  Created by Martin Matějka on 13.01.2021.
//

import SwiftUI

public struct BannerView: View {
    
    public init() {
        
    }
    
    public var body: some View {


        VStack {

            Text(sharedText: Resource.Text.movies)
                .font(.largeTitle)

            Text(sharedText: Resource.Text.button_save)
                .font(.largeTitle)

            Text(Resource.Text.genre.localized)
                .font(.largeTitle)
        }
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        BannerView()
            .previewLayout(.fixed(width: 500, height: 500))
            .environment(\.locale, .init(identifier: "cs"))

        BannerView()
            .previewLayout(.fixed(width: 500, height: 500))
            .environment(\.locale, .init(identifier: "en"))

        BannerView()
            .previewLayout(.fixed(width: 500, height: 500))
            .environment(\.locale, .init(identifier: "sk"))
    }
}
