//
//  Log.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

final class Log {
    static func write(_ text: String) {
        #if DEBUG
        NSLog(text)
        #endif
    }
}
