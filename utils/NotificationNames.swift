//
//   NotificationNames.swift
//  StreamCinema.atv
//
//  Created by SCC on 27/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
extension Notification.Name {
    static let MovieUpdate = Notification.Name("MovieUpdate")
}
