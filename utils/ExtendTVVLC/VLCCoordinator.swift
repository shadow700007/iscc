//
//  VLCCoordinator.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import TVVLCKit
import Combine


// TODO: Is it still needed?
protocol VLCCoordinatorDelegate: AnyObject {
    func numberOfVideos(_ coordinator:VLCCoordinator) -> Int
    func coordinator(_ coordinator:VLCCoordinator, data atIndex:Int) -> SCCMovie
    func coordinator(_ coordinator:VLCCoordinator, watched forData:SCCMovie) -> SccWatched?
    func coordinator(_ coordinator: VLCCoordinator, infoPanel forData: SCCMovie) -> InfoPanelData
    func coordinator(_ coordinator: VLCCoordinator, update watched: SccWatched)
}

final class VLCCoordinator: NSObject {

    private let rootViewController: UINavigationController
    private let appData: AppData
    private var viewModel: VLCPlayerViewModel?
    private var playerController: VLCPlayerViewController?
    private var cancelables: Set<AnyCancellable> = Set()

    init(appData: AppData, rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
        self.appData = appData
        super.init()
    }

    func start() {
        self.showStreams()
    }
    
    private func showStreams() {
        guard let viewModel = self.viewModel else { return }
        viewModel.fetchStreams()
            .sink(receiveCompletion: { [weak self] completion in
                guard case .failure(let error) = completion else { return }
                self?.rootViewController.presentErrorAlert(error: error)
            }) { [weak self] (streams) in
                self?.showAlertWithList(of: streams)
            }.store(in: &cancelables)
    }
    
    private func showAlertWithList(of streams: VideoStreams) {
        Log.write("showAlert streamsCount:\(streams.data.count)")
        let selectedStream = streams.isAutoSelectVideo()
        let actions = streams.getSortedData().map { stream -> UIAlertAction in
            let text = stream.getStreamInfo(isSselected: selectedStream?.id == stream.id)
            let action = UIAlertAction(title: text, style: .default) { [weak self] _ in
                Log.write("[VLCPlayer]: Selected stream: \(stream.ident ?? "")")
                self?.prepare(stream: stream)
            }
            return action
        }
        self.rootViewController.presentAlertActionSheet(title: String(localized: .chooseStream), message: nil, actions: actions, completion: nil)
    }
    
    private func prepare(stream: VideoStream) {
        guard let viewModel = self.viewModel else { return }
        if let isHDR = stream.video?.contains(where: { $0.isHdr }),
           isHDR {
            self.viewModel?.range = .HDR
        } else if let isDvhe = stream.video?.contains(where: { $0.codec?.lowercased() == "dvhe" }), isDvhe {
            self.viewModel?.range = .DolbyVision
        } else {
            self.viewModel?.range = .SDR
        }
        viewModel
            .getLink(for: stream)
            .compactMap { model -> URL? in
                guard let url = model.createLink() else { Log.write("[VLCPlayer]: Create URL failed!"); return nil }
                return url
            }
            .sink { [weak self] (completion) in
                guard case .failure(let error) = completion else { return }
                self?.rootViewController.presentErrorAlert(error: error)
            } receiveValue: { [weak self] url in
                Log.write("[VLCPlayer]: Play next succeded.")
                self?.viewModel?.link = url
                self?.vlcPlay()
            }.store(in: &cancelables)
    }
    
    private func vlcPlay() {
        if let viewModel = self.viewModel {
            self.playerController = VLCPlayerViewController.create(viewModel: viewModel)
            self.rootViewController.present(self.playerController!, animated: true)
        }
    }

    func presentVLCPlayer(modelData: MovieModel, startAt: Int, isContinue: Bool = false) {
        self.presentVLCPlayer(modelData: modelData, startAt: startAt, isContinue: isContinue, title: nil, logo: nil, desc: nil)
    }

    private func presentVLCPlayer(modelData: MovieModel, startAt: Int, isContinue: Bool, title: String? = nil, logo: URL? = nil, desc: String? = nil) {
        let viewModel = VLCPlayerViewModel(
            modelData: modelData,
            startAt: startAt,
            isContinue: isContinue,
            appData: appData
        )
        self.viewModel = viewModel
    }

//    private func getMedia(from url:URL) -> VLCMedia {
//        let media = VLCMedia(url: url)
//        media.addOptions(["network-caching":5000, "freetype-rel-fontsize":20])
//        return media
//    }
//

}

