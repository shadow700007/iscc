//
//  SelectableTableViewCell.swift
//  TVVLCPlayer
//
//  Created by Jérémy Marchand on 30/12/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit

final class SelectableTableViewCell: UITableViewCell {
    @IBOutlet var label: UILabel!
    @IBOutlet var checkmarkView: UIImageView!
    
    public func updateFont() {
        if self.checkmarkView.isHidden {
            self.label.font = UIFont.systemFont(ofSize: 31, weight: .regular)
        } else {
            self.label.font = UIFont.systemFont(ofSize: 31, weight: .semibold)
        }
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        if context.nextFocusedView === self {
            coordinator.addCoordinatedAnimations({
                self.label.textColor = .white
                self.checkmarkView.tintColor = .white
                self.label.font = UIFont.systemFont(ofSize: 31, weight: .bold)
            }, completion: nil)
        }
        else {
            coordinator.addCoordinatedAnimations({
                self.label.textColor = .lightGray
                self.checkmarkView.tintColor = .lightGray
                self.updateFont()
            }, completion: nil)
        }
    }
}
