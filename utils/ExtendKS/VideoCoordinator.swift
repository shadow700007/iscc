////
////  VideoCoordinator.swift
////  StreamCinema.atv
////
////  Created by SCC on 05/08/2020.
////  Copyright © 2020 SCC. All rights reserved.
////
//
//import UIKit
//import KSPlayer
//
//protocol VideoCoordinatorDelegate: class {
//    func numberOfVideos(_ coordinator:VideoCoordinator) -> Int
//    func coordinator(_ coordinator:VideoCoordinator, data atIndex:Int) -> InfoData
//    func coordinator(_ coordinator:VideoCoordinator, watched forData:InfoData) -> SccWatched?
//    func coordinator(_ coordinator: VideoCoordinator, infoPanel forData: InfoData) -> InfoPanelData
//    func coordinator(_ coordinator: VideoCoordinator, update watched: SccWatched)
//}
//
//final class VideoCoordinator {
//    public weak var delegate:VideoCoordinatorDelegate?
//    
//    private var currentIndex: Int = 0
//    private var count:Int = 0
//    private var movieData: InfoData?
//    private var infoPanel: InfoPanelData?
//    private var sender:UIViewController?
//    private weak var playerVC:PlayerViewController?
//    private var nextEpisodneControlTimer: Timer?
//    private var continueAudioInLang: String?
//    private var continueTitleInLang: String?
//    private var watchedData: SccWatched?
//    private var osSubtitles: [OSDownload] = [] {
//        didSet {
//            self.playerVC?.externalSubtitles = self.osSubtitles
//        }
//    }
//    private var isContrinue: Bool = false
//    
//    public func set(_ sender: UIViewController, startAt index: Int, isContrinue:Bool) {
//        self.currentIndex = index
//        self.sender = sender
//        self.isContrinue = isContrinue
//        if self.prepareData() {
//            self.showStreamList()
//        }
//    }
//    
//    private func prepareData() -> Bool {
//        guard let count = self.delegate?.numberOfVideos(self) else {
//            Log.write("prepareData delegate not exists")
//            return false
//        }
//        self.setContinuteLang()
//        self.count = count
//        Log.write("prepareData count:\(count) currentIndex:\(self.currentIndex)")
//        if self.count > 0,
//            self.currentIndex < self.count {
//            self.getData(at: self.currentIndex)
//            return true
//        } else {
//            self.movieData = nil
//            self.watchedData = nil
//            return false
//        }
//    }
//    
//    private func setContinuteLang() {
//        if let audioTracks = self.playerVC?.player?.getAudioTracks() {
//            for track in audioTracks {
//                if track.isEnabled {
//                    self.continueAudioInLang = track.language ?? track.name
//                }
//            }
//        }
//    
//        if let subtitleView = self.playerVC?.player?.srtControl.view as? KSSubtitleView,
//           subtitleView.selectedInfo.wrappedValue.name != "no show subtitle" {
//            self.continueTitleInLang = subtitleView.selectedInfo.wrappedValue.name
//        }
//    }
//    
//    private func getData(at index:Int) {
//        guard let delegate = self.delegate else { return }
//        let data = delegate.coordinator(self, data: index)
//        let infoPanel = delegate.coordinator(self, infoPanel: data)
//        guard let watched = delegate.coordinator(self, watched: data)  else { return }
//        Log.write("getData exist")
//        self.movieData = data
//        self.watchedData = watched
//        self.infoPanel = infoPanel
//    }
//    
//    public func showStreamList() {
//        guard let data = self.movieData else { return }
//        Log.write("showStreamList for originaltitle: \(data.source?.infoLabels?.originaltitle ?? "none")")
//        self.getStreams(from: data) { [weak self] result in
//            guard let self = self else { return }
//            switch result {
//            case .success(let streams):
//                self.showAlert(for: streams)
//            case .failure(let error):
//                error.handleError(on: self.sender)
//            }
//        }
//    }
//    
//    private func showAlert(for streams:VideoStreams) {
//        Log.write("showAlert streamsCount:\(streams.data.count)")
//        let alert = UIAlertController(title: "choose-the-stream".localizable, message: nil, preferredStyle: .alert)
//        let selectedStream = streams.isAutoSelectVideo()
//        self.prepereSubtitles()
//        for stream in streams.getSortedData() {
//            let text = stream.getStreamInfo(isSselected: selectedStream?.id == stream.id)
//            let action = UIAlertAction(title: text, style: .default) {[weak self] _ in
//                guard let self = self else { return }
//                Log.write("showAlert selectedStream:\(stream.ident ?? "")")
//                stream.getLink { [weak self] result in
//                    guard let self = self else { return }
//                    self.prepareLinkFrom(result)
//                }
//            }
//            alert.addAction(action)
//        }
//        alert.addAction(UIAlertAction(title: "button_cancel".localizable, style: .cancel, handler: nil))
//        self.sender?.show(alert, sender: self)
//    }
//    
//    private func prepareLinkFrom(_ result: (Result<WSStreamModel, RequestErrors>)) {
//        Log.write("prepareLinkFrom")
//        switch result {
//        case .failure(let error):
//            error.handleError(on: self.sender)
//        case .success(let data):
//            if let link = data.link,
//                let videoURL = URL(string: link) {
//                self.invalidateTimer()
//                self.play(link: videoURL)
//            }
//        }
//    }
//    
//    private func play(link: URL) {
//        Log.write("play(link: \(link)")
//
//        if self.playerVC == nil {
//            self.playerVC = PlayerViewController.instantiate()
//            playerVC?.playerDelegate = self
//            self.sender?.present(playerVC!, animated: false, completion: nil)
//        }
//        self.playerVC?.externalSubtitles = self.osSubtitles
//        self.playerVC?.resource = KSPlayerResource(url: link)
//
//        guard let data = self.movieData,
//            let watched = self.watchedData,
//            let infoPanel = self.infoPanel else { return }
//        Log.write("play link dataExist")
//    
//        self.playerVC?.infoPanelData = infoPanel
//        let deadlineTime = DispatchTime.now() + .milliseconds(300)
//        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
//            self.playerVC?.play(watched: watched, mediaInfo: data, Media: 0, isContinue: self.isContrinue)
//            self.isContrinue = false
//        }
//
//        guard let delegate = UIApplication.shared.delegate as? AppDelegate,
//                let mediaId = data.id else { return }
//        delegate.appData.scService.setAsPlayed(mediaID: mediaId)
//    }
//    
//    private func getStreams(from data:InfoData, completion: @escaping (Result<VideoStreams, RequestErrors>) -> Void) {
//        guard let delegate = UIApplication.shared.delegate as? AppDelegate,
//            let movieID = data.id else {
//                completion(.failure(.noDataForRequest))
//                return
//        }
//        delegate.appData.scService.getStreams(for: movieID, completion: completion)
//    }
//    
//    private var isTimeToShowNexEpisode:Bool {
//        get {
//            if let time = self.playerVC?.player?.currentTime,
//               time > 0,
//                let total = self.playerVC?.player?.totalTime,
//                (20 + time) >= total {
//                Log.write("isTimeToShowNexEpisode")
//                return true
//            }
//            return false
//        }
//    }
//    
//    private func showNextEpisodeView() {
//        Log.write("showNextEpisodeView")
//        if self.nextEpisodneControlTimer == nil {
//            self.playerVC?.hideCountDownView(false)
//            var countDownTime = 20
//            self.playerVC?.updateNextVideo(countDown: countDownTime)
//            self.playerVC?.setNextVideo(title: self.infoPanel?.title)
//            self.playerVC?.setNextVideo(poster: self.infoPanel?.posterURL)
//            Log.write("showNextEpisodeView prepare timer")
//            self.nextEpisodneControlTimer = .scheduledTimer(withTimeInterval: 1, repeats: true, block: {[weak self] timer in
//                guard let self = self else { return }
//                countDownTime -= 1
//                Log.write("showNextEpisodeView timer countDownTime:\(countDownTime)")
//                self.playerVC?.updateNextVideo(countDown: countDownTime)
//                if countDownTime == 1 {
//                    self.playerVC?.hideCountDownView(true)
//                    self.invalidateTimer()
//                    Log.write("showNextEpisodeView hideCountDownView")
//                } else if countDownTime == 2 {
//                    self.currentIndex += 1
//                    if !self.prepareData() {
//                        self.dismiss()
//                        Log.write("showNextEpisodeView dismiss")
//                    } else {
//                        guard let data = self.movieData else { return }
//                        self.playNext(data)
//                        Log.write("showNextEpisodeView playNext")
//                    }
//                }
//            })
//        }
//    }
//    
//    private func playNext(_ data: InfoData) {
//        Log.write("playNext dataExists")
//        self.prepereSubtitles()
//        self.getStreams(from: data) { [weak self] result in
//            guard let self = self else { return }
//            switch result {
//            case .success(let streams):
//                Log.write("playNext getStreams result success")
//                let stream = streams.getVideoStream(audioLang: AudioLanguage(audioString:self.continueAudioInLang))
//                stream?.getLink(completion: { [weak self] result in
//                    guard let self = self else { return }
//                    self.prepareLinkFrom(result)
//                })
//            case .failure(let error):
//                Log.write("playNext getStreams result failure")
//                self.dismiss(error)
//            }
//        }
//    }
//    
//    private func dismiss(_ withError: RequestErrors? = nil) {
//        self.invalidateTimer()
//        self.playerVC?.dismiss(animated: true, completion: { [weak self] in
//            guard let self = self else { return }
//            self.playerVC?.player?.resetPlayer()
//            withError?.handleError(on: self.sender)
//        })
//    }
//    
//    private func invalidateTimer() {
//        self.nextEpisodneControlTimer?.invalidate()
//        self.nextEpisodneControlTimer = nil
//    }
//    
//    private func prepereSubtitles() {
//        if LoginManager.osTokenHash != nil {
//            self.osSubtitles.removeAll()
//            guard let delegate = UIApplication.shared.delegate as? AppDelegate,
//                  let imdb = self.movieData?.source?.services?.imdb else { return }
//            delegate.appData.osService.find(imdbid: imdb, lang: ["sk","cs","en"]) { result in
//                switch result {
//                case .success(let data):
//                    self.osSubtitles = data
//                case .failure(let error):
//                    error.handleError(on: self.playerVC)
//                }
//            }
//        }
//    }
//}
//
//extension VideoCoordinator: PlayerViewDelegate {
//    func playerViewWillDisapper(_ playerView: PlayerViewController) {
//        self.invalidateTimer()
//    }
//    
//    func playerView(_ playerView: PlayerViewController, didSave info: SccWatched) {
//        SccWatchedData.save(new: info)
//        self.delegate?.coordinator(self, update: info)
//        Log.write("playerView(_ playerView: PlayerViewController, didSave info: Watched)")
//    }
//    
//    func playerView(_ playerView: PlayerViewController, prepare action: PlayerAction) {
//        Log.write("playerView(_ playerView: PlayerViewController, prepare action: PlayerAction)")
//        if action == .didEndPlaying {
//            self.dismiss()
//        }
//    }
//    
//    func playerView(_ playerView: PlayerViewController, playerDidTimeChanged: TimeInterval) {
//         Log.write("playerView(_ playerView: PlayerViewController, playerDidTimeChanged: MediaPlayer)")
//        if let state = playerView.state, state != .error, self.isTimeToShowNexEpisode {
//            self.showNextEpisodeView()
//        }
//    }
//    
//    func playerViewLastAudio(_ playerView: PlayerViewController) -> String {
//        if let lang = self.continueAudioInLang {
//            return lang
//        }
//        
//        if let systemLangiso639 = Locale.current.iso639_2LanguageCode {
//            return systemLangiso639
//        }
//        
//        return "eng"
//    }
//}
